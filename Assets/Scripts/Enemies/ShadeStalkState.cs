﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadeStalkState : State
{
    private Shade shade;
    private float nextHitTime, nextPathUpdate;

    public ShadeStalkState(NPC owner) : base(owner)
    {
        shade = (Shade)owner ?? throw new System.ArgumentException();
    }

    public override float CalculateDesirability()
    {
        return (shade.targetScav != null) ? 1f : -1f;
    }

    public override void Enter()
    {
        Debug.Log("Shade attacking " + shade.targetScav.photonView.Owner.NickName);
        owner.NavAgent.isStopped = false;
    }
    
    public override void Exit()
    {
        Debug.Log("Shade stopped stalking");
        owner.NavAgent.isStopped = true;
    }

    public override void Update()
    {
        owner.NavAgent.isStopped = false;
        if (shade.targetScav != null)
        {
            if(Time.time > nextPathUpdate)
            {
                owner.NavAgent.SetDestination(shade.targetScav.transform.position);
                owner.NavAgent.speed = shade.IsLit ? shade.litStalkSpeed : shade.stalkSpeed;
                nextPathUpdate = Time.time + 0.2f;
            }

            if (owner.NavAgent.speed < 0.05f)
            {
                Vector3 dir = (shade.targetScav.transform.position - owner.transform.position).normalized;
                owner.transform.rotation = Quaternion.Lerp(owner.transform.rotation, Quaternion.LookRotation(dir, Vector3.up), Time.deltaTime * 5f);
            }
        }
        else
        {
            owner.NavAgent.speed = shade.IsLit ? shade.litWanderSpeed : shade.wanderSpeed;
        }        

        DamageWithinDistance();
    }

    private void DamageWithinDistance()
    {
        bool attacked = false;
        if(Time.time > nextHitTime)
        {
            nextHitTime = Time.time + shade.hitInterval;
            foreach (var scav in shade.scavsInVicinity)
            {
                float dist = Vector3.Distance(scav.transform.position, owner.transform.position);
                if(dist < shade.damageDistance)
                {
                    scav.Hit();
                    if(!attacked)
                        shade.OnAttack.Invoke();
                    attacked = true;
                }
            }
        }
    }
}
