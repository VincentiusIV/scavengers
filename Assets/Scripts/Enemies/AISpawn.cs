﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISpawn : MonoBehaviour
{
    public float minRespawnTime = 5f, maxRespawnTime = 15f;

    private List<Scavengers> scavsInVicinity = new List<Scavengers>();
    private Queue<Actor> actorsInWait = new Queue<Actor>();
    private float spawnTimer;

    public bool IsEmpty()
    {
        return scavsInVicinity.Count == 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        Scavengers scav = other.GetComponentInParent<Scavengers>();
        if (scav != null && !scavsInVicinity.Contains(scav))
            scavsInVicinity.Add(scav);
    }

    private void OnTriggerExit(Collider other)
    {
        Scavengers scav = other.GetComponentInParent<Scavengers>();
        if (scav != null && scavsInVicinity.Contains(scav))
            scavsInVicinity.Remove(scav);
    }

    public void TeleportAndWait(Actor actor)
    {
        if(!actorsInWait.Contains(actor))
        {
            actor.gameObject.SetActive(false);
            actorsInWait.Enqueue(actor);
            Debug.LogFormat("Spawn {0} queued {1}", name, actor.name);
        }
    }

    private void Update()
    {
        if(actorsInWait.Count > 0)
        {
            spawnTimer -= Time.deltaTime;
            if(spawnTimer <= 0f)
            {
                spawnTimer = UnityEngine.Random.Range(minRespawnTime, maxRespawnTime);
                Actor actor = actorsInWait.Dequeue();
                actor.transform.position = transform.position;
                actor.gameObject.SetActive(true);
                Debug.LogFormat("Respawned {1} at {0}", name, actor.name);
            }
        }
    }
}
