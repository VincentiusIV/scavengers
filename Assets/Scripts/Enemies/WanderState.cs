﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[System.Serializable]
public class WanderSettings
{
    public float minWait = 1f, maxWait = 2f;
    public float wanderRadius = 10f;
    public float maxTimePerWander = 10f;
}

public class WanderState : State
{
    private float timer;
    private float nextWanderTime;
    private bool isWaiting;
    private Vector3 currentDestination;

    public WanderState(NPC owner) : base(owner)
    {
    }

    public override float CalculateDesirability()
    {
        return 0.1f;
    }

    public override void Enter()
    {
        PickNewDestination();
    }

    private void PickNewDestination()
    {
        Vector3 randomPosition = owner.transform.position + Random.insideUnitSphere * owner.wanderSettings.wanderRadius;
        NavMeshHit hit;
        for (int i = 0; i < 30; i++)
        {
            if (NavMesh.SamplePosition(randomPosition, out hit, 1f, NavMesh.AllAreas))
            {
                timer = 0f;
                isWaiting = false;
                currentDestination = hit.position;
                owner.NavAgent.isStopped = false;
                owner.NavAgent.SetDestination(currentDestination);
                break;
            }
        }
    }

    public override void Exit()
    {
        owner.NavAgent.isStopped = true;
    }

    public override void Update()
    {
        timer += Time.deltaTime;
        if (owner.NavAgent.isStopped || timer > owner.wanderSettings.maxTimePerWander)
        {
            SetIsWaiting();
            if (timer >= nextWanderTime)
            {
                PickNewDestination();
            }
        }
    }

    private void SetIsWaiting()
    {
        if (!isWaiting)
        {
            nextWanderTime = timer + Random.Range(owner.wanderSettings.minWait, owner.wanderSettings.maxWait);
            owner.NavAgent.isStopped = true;
            isWaiting = true;
        }
    }
}
