﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC : Actor
{
    public StateMachine Brain { get; private set; }
    public NavMeshAgent NavAgent { get; private set; }
    public TriggerEventListener enemyProximity;
    public WanderSettings wanderSettings;
    public LayerMask blockingLayer;
    public float overlookChanceIfCrouching = 0.5f;
    public float velocityToAnimMult = 1.0f, animSpeedMultiplier = 1f, dampTime = 0.5f;

    protected override void Awake()
    {
        base.Awake();
        NavAgent = GetComponent<NavMeshAgent>();
        Brain = new StateMachine(new WanderState(this));
    }
    
    protected virtual void Update()
    {
        if(photonView.IsMine)
            Brain.Update();

        Vector3 worldVelocity = transform.InverseTransformVector(NavAgent.velocity) * velocityToAnimMult;
        float xVelocity = worldVelocity.x;
        animator.SetFloat("X_VELOCITY", xVelocity, dampTime, Time.fixedDeltaTime);
        float yVelocity = worldVelocity.y;
        animator.SetFloat("Y_VELOCITY", yVelocity, dampTime, Time.fixedDeltaTime);
        float zVelocity = worldVelocity.z;
        animator.SetFloat("Z_VELOCITY", zVelocity, dampTime, Time.fixedDeltaTime);
        animator.SetBool("IsGrounded", true);
        animator.SetBool("IsCrouching", false);
        animator.SetBool("IsDead", !IsAlive);
    }
}
