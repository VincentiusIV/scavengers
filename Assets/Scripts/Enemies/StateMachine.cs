﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class StateMachine
{
    private List<State> States { get; set; } = new List<State>();
    public State CurrentState { get; private set; }
    public State DefaultState { get; private set; }
    public bool IsActive { get; private set; }
    public StateMachine(State defaultState)
    {
        DefaultState = defaultState ?? throw new ArgumentNullException();
        CurrentState = DefaultState;
        States.Add(DefaultState);
        Start();
    }

    public void AddState<T>(T state)
        where T : State
    {
        if (HasStateOfType(state.GetType()))
            return;
        States.Add(state);
    }

    public void Update()
    {
        CurrentState.Update();
        Arbitrate();
    }

    private void Arbitrate()
    {
        float highest = float.MinValue;
        State mostDesirableState = CurrentState;
        foreach (var state in States)
        {
            float desirability = state.CalculateDesirability();
            if (desirability > highest)
            {
                highest = desirability;
                mostDesirableState = state;
            }
        }
        ChangeTo(mostDesirableState);
    }

    public State ChangeState<T>()
        where T : State
    {
        State state = DefaultState;
        for (int i = 0; i < States.Count; i++)
        {
            if (States[i].GetType() == typeof(T))
            {
                state = States[i];
                break;
            }
        }

        return ChangeTo(state);
    }

    private State ChangeTo(State state)
    {
        if (state == CurrentState)
            return CurrentState;

        CurrentState.Exit();
        CurrentState = state;
        CurrentState.Enter();
        return CurrentState;
    }

    public void ChangeToDefaultState()
    {
        ChangeTo(DefaultState);
    }

    public void Start()
    {
        if (IsActive)
            return;
        CurrentState.Enter();
        IsActive = true;
    }

    public void Terminate()
    {
        if (!IsActive)
            return;
        CurrentState.Exit();
        IsActive = false;
    }

    public bool HasStateOfType(Type type)
    {
        for (int i = 0; i < States.Count; i++)
        {
            if (States[i].GetType() == type)
            {
                return true;
            }
        }
        return false;
    }

    public void DrawGizmos()
    {
        CurrentState.DrawGizmos();
    }
}
