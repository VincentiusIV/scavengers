﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public abstract class State
{
    protected NPC owner;
    public State(NPC owner)
    {
        this.owner = owner;
    }
    public abstract float CalculateDesirability();
    public abstract void Enter();
    public abstract void Exit();
    public abstract void Update();
    public virtual void DrawGizmos()
    {

    }
}
