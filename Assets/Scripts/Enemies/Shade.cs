﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using System;

public interface IShinable
{
    void Lit();
    void Unlit();
}

public class Shade : NPC, IShinable
{
    public UnityEvent OnAttack, OnStartStalking, OnStoppedStalking, OnTeleport;
    public bool IsLit => litCounter > 0;
    private int litCounter = 0;
    public float scanInterval = 0.33f;
    public float hitInterval = 0.33f;
    public float damageDistance = 1f;
    public bool provokedByLit = false, canOpenDoors = true;
    public float litTimeBeforeTeleport = 5;
    public Transform headPos;
    public LayerMask obstacleMask;
    public float loseSightTime = 5f;
    public float wanderSpeed = 2f, stalkSpeed = 5f, litWanderSpeed = 2f, litStalkSpeed = 5f;
    public AISpawn[] availableSpawns;

    public List<Scavengers> scavsInVicinity { get; private set; } = new List<Scavengers>();
    public Scavengers targetScav { get; private set; }
    private IEnumerator searchRoutine;
    private float litTimer = 0f;
    private float lineOfSightTimer = 0f;

    protected override void Awake()
    {
        base.Awake();
        Brain.AddState(new ShadeStalkState(this));
    }

    private void OnEnable()
    {
        if (searchRoutine != null)
            StopCoroutine(searchRoutine);
        searchRoutine = SearchRoutine();
        StartCoroutine(searchRoutine);
    }

    private IEnumerator SearchRoutine()
    {
        while (photonView.IsMine)
        {
            UpdateNearbyScavengers();
            if (targetScav != null && !scavsInVicinity.Contains(targetScav))
                StopStalking();

            if (targetScav == null || !targetScav.IsAlive || !scavsInVicinity.Contains(targetScav))
            {
                if(scavsInVicinity.Count > 0)
                {
                    if (DetermineTargetScav())
                    {
                        Debug.LogFormat("{0} started stalking {1}", name, targetScav.name);
                        OnStartStalking.Invoke();
                    }
                }
            }

            yield return new WaitForSeconds(scanInterval);
            
            if (targetScav != null)
            {
                if (!HasLineOfSightTo(targetScav.playerCamera.transform))
                {
                    lineOfSightTimer += scanInterval;
                    Debug.LogFormat("{0} cannont see scav {1}, timer: {2}", name, targetScav.name, lineOfSightTimer);

                    if (lineOfSightTimer > loseSightTime)
                    {
                        Debug.LogFormat("{0} lost track of {1}", name, targetScav.name);
                        StopStalking();
                    }
                }
                else
                {
                    lineOfSightTimer = 0f;
                    Debug.LogFormat("{0} can see scav {1}", name, targetScav.name);
                }
            }
        }
    }

    private void StopStalking()
    {
        Debug.LogFormat("{0} stopped stalking {1}", name, targetScav.name);
        targetScav = null;
        OnStoppedStalking.Invoke();
        lineOfSightTimer = 0f;
    }

    private bool DetermineTargetScav()
    {
        if (provokedByLit && litCounter == 0)
            return false;

        bool foundSmth = false;
        foreach (var scav in scavsInVicinity)
        {
            if (scav == null || !scav.IsAlive)
                continue;
            if (scav.GetComponent<Movement>().IsCrouching && UnityEngine.Random.value > overlookChanceIfCrouching * scanInterval)
                continue;

            if (!HasLineOfSightTo(scav.playerCamera.transform))
                continue;

            foundSmth = true;
            targetScav = scav;
            lineOfSightTimer = 0f;
            Debug.LogFormat("[{0}] picked new target {1}", this.name, targetScav.name);
            break;
        }
        if (!foundSmth) targetScav = null;
        return foundSmth;
    }

    private bool HasLineOfSightTo(Transform target)
    {
        float maxDist = Vector3.Distance(target.position, headPos.position);
        RaycastHit hit;
        if (Physics.Raycast(headPos.position, headPos.forward, out hit, maxDist, obstacleMask))
        {
            return hit.transform == target;
        }
        return true;
    }

    private void UpdateNearbyScavengers()
    {
        scavsInVicinity.Clear();
        foreach (var item in enemyProximity.withinTrigger)
        {
            Scavengers scav = item.GetComponentInParent<Scavengers>();
            if (scav != null && !scavsInVicinity.Contains(scav))
                scavsInVicinity.Add(scav);
        }
    }


    public void Lit()
    {
        photonView.RPC("LitRpc", RpcTarget.All);
    }

    [PunRPC]
    protected void LitRpc()
    {
        ++litCounter;
        Debug.Log("Lit da shade");
    }

    public void Unlit()
    {
        photonView.RPC("UnlitRpc", RpcTarget.All);
    }

    [PunRPC]
    protected void UnlitRpc()
    {
        --litCounter;
        Debug.Log("unuLit da shade");
    }

    [PunRPC]
    protected override void KillRpc()
    {
        base.KillRpc();
        gameObject.SetActive(false);
    }

    [PunRPC]
    protected override void RespawnRpc()
    {
        base.RespawnRpc();
        gameObject.SetActive(true);
    }

    protected override void Update()
    {
        base.Update();
        if (IsLit && litTimeBeforeTeleport > 0)
        {
            litTimer += Time.deltaTime;
            if(litTimer >= litTimeBeforeTeleport)
            {
                TeleportToAnotherArea();
                litTimer = 0f;
            }
        }
        else
        {
            litTimer = 0f;
        }
    }

    private void TeleportToAnotherArea()
    {
        if (availableSpawns.Length == 0) return;
        int randIdx = UnityEngine.Random.Range(0, availableSpawns.Length);
        for (int i = 0; i < availableSpawns.Length; i++)
        {
            int idx = (randIdx + i) % availableSpawns.Length;
            AISpawn spawn = availableSpawns[i];
            if(spawn.IsEmpty())
            {
                spawn.TeleportAndWait(this);
                return;
            }
        }
    }

    private void OnValidate()
    {
        scanInterval = Mathf.Max(scanInterval, 0.05f);
        hitInterval = Mathf.Max(hitInterval, 0.05f);
    }
}
