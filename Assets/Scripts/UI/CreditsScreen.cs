﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class CreditsScreen : UIScreen
{
    public TMP_Text missionResultTf;
    public Button quitButton;
    private bool quitPressed;

    protected override void Awake()
    {
        base.Awake();
        quitButton.onClick.AddListener(OnQuitClick);
    }

    public void HandleCredits(string missionResult, string reason)
    {
        missionResultTf.text = string.Format(missionResultTf.text, missionResult, reason);
    }

    private void OnQuitClick()
    {
        Application.Quit();
    }
}
