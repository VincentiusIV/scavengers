﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectateCamera : MonoBehaviour
{
    private static List<SpectateCamera> cameras = new List<SpectateCamera>();
    private static int currentIdx = 0;
    public static SpectateCamera CurrentSpectator => cameras[currentIdx];
    public static Actor CurrentSpectatedActor => CurrentSpectator.owner;
    public Actor owner;
    private Camera cameraComponent;
    private AudioListener audioListener;
    private AimUpdate aimUpdate;
    private Flashlight flashLight;

    private void Awake()
    {
        cameras.Add(this);
        cameraComponent = GetComponent<Camera>();
        audioListener = GetComponent<AudioListener>();
        aimUpdate = GetComponent<AimUpdate>();
        flashLight = GetComponentInChildren<Flashlight>();
    }

    public static void Next()
    {
        cameras[currentIdx].Disable();
        currentIdx = (currentIdx + 1) % cameras.Count;
        cameras[currentIdx].Enable();
    }

    public static void Previous()
    {
        cameras[currentIdx].Disable();
        currentIdx = (currentIdx - 1 + cameras.Count) % cameras.Count;
        cameras[currentIdx].Enable();
    }

    public void Enable()
    {
        cameraComponent.enabled = true;
        audioListener.enabled = true;
        aimUpdate.enabled = true;
        flashLight.DisableVolumetricLights();
        currentIdx = cameras.IndexOf(this);
    }

    public void Disable()
    {
        cameraComponent.enabled = false;
        audioListener.enabled = false;
        aimUpdate.enabled = false;
        flashLight.EnableVolumetricLights();
    }

    private void OnDestroy()
    {
        cameras.Remove(this);
    }
}
