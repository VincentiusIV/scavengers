﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemExchangeUI : UIScreen
{
    private static ItemExchangeUI instance;
    public InventoryUI playerUI;
    public InventoryUI otherUI;

    protected override void Awake()
    {
        base.Awake();
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    public override void Init(Actor actor)
    {
        base.Init(actor);
        playerUI.Show(actor.GetComponent<InventoryComponent>());
        playerUI.gameObject.SetActive(true);
        otherUI.gameObject.SetActive(false);
    }

    public override bool ChangeVisibility(bool newState)
    {
        bool s = base.ChangeVisibility(newState);
        if (!s)
            otherUI.gameObject.SetActive(false);
        if (IsVisible)
            actor.LockControl();
        else
            actor.UnlockControl();
        playerUI.itemInspector.Hide();
        return IsVisible;
    }

    public static void ShowOther(InventoryComponent inventory)
    {
        instance.otherUI.Show(inventory);
        instance.otherUI.gameObject.SetActive(true);
    }
}
