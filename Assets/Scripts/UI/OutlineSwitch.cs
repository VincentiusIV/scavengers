﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineSwitch : MonoBehaviour
{
    public Material outlineMaterial;
    public OutlineSwitch[] children;
    public bool disableOutlineRenderer = true;
    private MeshRenderer meshRenderer;

    private Material[] materialsWithOutline;
    private Material[] materials;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.enabled = !disableOutlineRenderer;
        materials = meshRenderer.materials;
        materialsWithOutline = new Material[meshRenderer.materials.Length];
        for (int i = 0; i < meshRenderer.materials.Length; i++)
        {
            materialsWithOutline[i] = outlineMaterial;
        }
        HideOutline();

        Interactable interactable = GetComponentInParent<Interactable>();
        if(interactable)
        {
            interactable.OnSelectEvent.AddListener(delegate(Actor actor) { ShowOutline(); });
            interactable.OnDeselectEvent.AddListener(delegate (Actor actor) { HideOutline(); });
        }
    }

    public void ShowOutline()
    {
        if(meshRenderer)
        {
            meshRenderer.materials = materialsWithOutline;
            meshRenderer.enabled = true;
        }
        foreach (var c in children) {
            c.ShowOutline();
        }
    }

    public void HideOutline()
    {
        if (meshRenderer)
        {
            meshRenderer.materials = materials;
            meshRenderer.enabled = !disableOutlineRenderer;
        }
        foreach (var c in children) {
            c.HideOutline();
        }
    }
}
