﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// Draggable icon used to move stuff around in the inventory.
/// </summary>
public class ItemIcon : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Image iconImage;
    public TMPro.TMP_Text amountTextfield;
    private RectTransform rectTransform;
    private CanvasScaler scaler;
    private Transform originalParent;
    public Slot itemStack { get; private set; } // the stack this icon is representing.
    private Transform dragParent;
    private bool isDragging;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        scaler = GetComponentInParent<CanvasScaler>();

        originalParent = transform.parent;
        dragParent = GetComponentInParent<ItemExchangeUI>().visualRoot.transform;

        Debug.Assert(iconImage);
        Debug.Assert(amountTextfield);
    }

    public void ShowItemStack(Slot itemStack)
    {
        this.itemStack = itemStack;
        Debug.Assert(itemStack != null);

        iconImage.sprite = itemStack.Contents.Item.Icon;
        iconImage.enabled = true;

        string amountText = itemStack.Amount > 1 ? itemStack.Amount.ToString() : "";
        amountTextfield.text = amountText;
    }

    public void Empty()
    {
        iconImage.sprite = null;
        iconImage.enabled = false;
        amountTextfield.text = "";

        this.itemStack = null;
    }

    public void Reset()
    {
        transform.SetParent(originalParent);
        if(rectTransform != null)
            rectTransform.localPosition = Vector3.zero;

        if(iconImage != null)
            iconImage.raycastTarget = true;

        isDragging = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (itemStack.IsEmpty || isDragging)
            return;

        string _name = !itemStack.IsEmpty ? itemStack.Contents.Item.Name : "null";
        //Debug.Log("Started dragging item: " + _name);

        iconImage.raycastTarget = false;
        isDragging = true;
        transform.SetParent(dragParent);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(isDragging)
            rectTransform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!isDragging)
            return;

        Reset();
    }
}
