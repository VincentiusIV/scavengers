﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AIMonitor : MonoBehaviour
{
    public Slider slider;
    public TMP_Text logTextField;

    private void Start()
    {
        SpaceshipManager.OnAISpeak.AddListener(WriteLog);
    }

    private void Update()
    {
        slider.value = SpaceshipManager.Instance.shipwreckManager.normDistance;
    }

    public void WriteLog(string text)
    {
        DateTime time = DateTime.Now;
        string finalText = string.Format("[{0}]: {1} \n", time.ToString("HH:mm"), text);
        logTextField.text = finalText + logTextField.text;
    }

    private void OnDestroy()
    {
        SpaceshipManager.OnAISpeak.RemoveListener(WriteLog);
    }
}
