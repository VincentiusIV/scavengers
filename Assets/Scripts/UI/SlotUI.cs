﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class SlotUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IDropHandler
{
    public Slot itemSlot { get; private set; }
    public bool ContainsStack { get { return (itemSlot != null && itemSlot.Amount > 0); } }
    public GameObject itemIconPrefab;
    protected ItemIcon itemIcon;
    private Button button;
    protected InventoryComponent inventory;
    private ItemInspector itemInspector;
    private bool pointerOver;

    private void Awake()
    {
        InitIfNecessary();
    }

    private void InitIfNecessary()
    {
        if(button == null)
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(OnClick);
        }

        if(itemIcon == null)
        {
            GameObject iconObject = Instantiate(itemIconPrefab, transform);
            itemIcon = iconObject.GetComponent<ItemIcon>();

            Debug.Assert(itemIcon);
            Debug.Assert(itemIconPrefab);
        }
    }

    public virtual void Setup(InventoryComponent inventory, Slot slot, ItemInspector itemInspector)
    {
        Debug.Assert(slot != null);
        Debug.Assert(itemInspector != null);

        this.inventory = inventory;

        if (this.itemSlot != null)
            this.itemSlot.Changed -= OnStackSizeChanged;

        this.itemSlot = slot;
        this.itemSlot.Changed += OnStackSizeChanged;

        this.itemInspector = itemInspector;

        UpdateSlotContents();
    }

    private void Update()
    {
        if(pointerOver && Input.GetMouseButtonUp(1))
        {
            inventory.DropSlotContents(itemSlot);
        }
    }

    protected virtual void UpdateSlotContents()
    {
        InitIfNecessary();

        if (itemSlot.IsEmpty)
        {
            itemIcon.Empty();
            return;
        }

        itemIcon.ShowItemStack(itemSlot);
        itemIcon.gameObject.SetActive(true);
    }

    protected virtual void OnStackSizeChanged(object sender, SlotEventArgs e)
    {
        UpdateSlotContents();
    }

    private void OnClick()
    {
        string _name = !itemSlot.IsEmpty ? itemSlot.Contents.Item.Name : "null";
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
            return;
        pointerOver = true;
        if (itemInspector != null)
            itemInspector.Inspect(itemSlot, transform.position);
        else
            Debug.Log("Reference to ItemInspector is null...");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pointerOver = false;
        if (itemInspector != null)
            itemInspector.Hide();
        else
            Debug.Log("Reference to ItemInspector is null...");
    }

    private void OnDestroy()
    {
        if(itemSlot != null)
            itemSlot.Changed -= OnStackSizeChanged;
    }

    public virtual void OnDrop(PointerEventData eventData)
    {
        if(eventData.pointerDrag == null)
            return;
        string _name = !itemSlot.IsEmpty ? itemSlot.Contents.Item.Name : "null";
        ItemIcon droppedIcon = eventData.pointerDrag.GetComponent<ItemIcon>();
        bool success = inventory.MoveItemFromTo(droppedIcon.itemStack, itemSlot);
        Debug.Assert(success);
    }

    public void Reset()
    {
        itemIcon?.Reset();
    }

    public virtual void Select()
    {
        if (itemSlot.IsEmpty)
            itemInspector.Hide();
        else
        {
            Vector3 offset = GetComponent<RectTransform>().sizeDelta / 2;
            offset.x *= -1f;
            itemInspector.Inspect(itemSlot, transform.position + offset);
        }
    }

    public virtual void Use()
    {

    }
}
