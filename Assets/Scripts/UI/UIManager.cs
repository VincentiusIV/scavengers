﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }
    public static bool CanUseUI { get; set; } = true;
    public Type defaultScreen = null;
    public GameObject root;
    public Actor Actor { get; private set; }
    private Dictionary<Type, UIScreen> screens;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    public void Observe(Actor actor)
    {
        this.Actor = actor;
        UIScreen[] _screens = transform.GetComponentsInChildren<UIScreen>();
        string screenNames = "";
        foreach (var screen in _screens)
        {
            screen.gameObject.SetActive(true);
            screenNames += screen.name + ", ";
        }
        screens = new Dictionary<Type, UIScreen>();
        for (int i = 0; i < _screens.Length; i++)
        {
            UIScreen current = _screens[i];
            if (!current.isRootScreen)
                continue;
            screens.Add(current.GetType(), current);
            current.Init(Actor);
            current.Hide();
        }
        SwitchScreen(defaultScreen);

        foreach (var pair in screens)
            pair.Value.Init(Actor);
    }

    public void Show(Type screenType)
    {
        if (screenType == null)
            return;
        if (!IsScreenVisible(screenType))
            SwitchScreen(screenType);
    }

    public void Hide(Type screenType)
    {
        if (IsScreenVisible(screenType))
            SwitchScreen(screenType);
    }

    public void SwitchScreen(Type screenType)
    {
        if (screenType == null || !screens.ContainsKey(screenType) || !CanUseUI)
            return;
        UIScreen screen = screens[screenType];
        if (!screen.IsVisible)
            HideAllScreensExcept(screenType);
        screen.ChangeVisibility(!screen.IsVisible);
        ShowDefaultScreenIfNecessary();
    }

    private void ShowDefaultScreenIfNecessary()
    {
        if (!IsAnyScreenVisible())
            Show(defaultScreen);
    }

    private void HideAllScreensExcept(Type screenType)
    {
        foreach (var pair in screens)
        {
            UIScreen current = pair.Value;
            if (current.GetType() != screenType && !current.alwaysActive && current.IsVisible)
                current.Hide();
        }
    }

    public UIScreen GetScreen(Type screenType)
    {
        if (screens != null && screens.ContainsKey(screenType))
            return screens[screenType];
        return null;
    }

    public bool IsScreenVisible(Type screenType)
    {
        if (screens == null || !screens.ContainsKey(screenType))
            return false;
        UIScreen screen = screens[screenType];
        return (screen.IsVisible);
    }

    public bool IsAnyScreenVisible()
    {
        bool result = false;
        foreach (var pair in screens)
        {
            result |= IsScreenVisible(pair.Key);
            if (result)
                break;
        }
        return (result);
    }

    public void Reset()
    {
        if(screens != null)
            foreach (var pair in screens)
                if(pair.Value.IsVisible)
                    pair.Value.ChangeVisibility(false);
    }

    public static void ShowUI()
    {
        Instance.root.SetActive(true);
    }

    public static void HideUI()
    {
        Instance.root.SetActive(false);
    }

    public static void ShowScreen(Type screenType)
    {
        Instance?.Show(screenType);
    }

    public static void HideScreen(Type screenType)
    {
        Instance?.Hide(screenType);
    }

    protected void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }
}
