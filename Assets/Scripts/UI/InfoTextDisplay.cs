﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoTextDisplay : MonoBehaviour
{
    private static InfoTextDisplay instance;

    public Text textField;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(this);
    }

    public static void Display(string text, float time = 3f)
    {
        instance.StopAllCoroutines();
        instance.StartCoroutine(instance.DisplayRoutine(text, time));   
    }

    private IEnumerator DisplayRoutine(string text, float time = 3f)
    {
        textField.enabled = true;
        textField.text = text;
        yield return new WaitForSeconds(time);
        textField.enabled = false;
    }
}
