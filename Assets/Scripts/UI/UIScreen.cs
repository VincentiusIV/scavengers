﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

public class UIScreen : MonoBehaviour
{
    public EventHandler OnShow, OnHide;
    public bool IsVisible { get; private set; } = true;
    public bool isRootScreen = true;
    public bool alwaysActive;
    public bool showCursorWhileActive = true;
    public GameObject defaultSelection;
    public GameObject visualRoot;
    protected Actor actor = null;
    private UIScreen[] subscreens;

    protected virtual void Awake()
    {
        if (!isRootScreen)
            return;
        subscreens = GetComponentsInChildren<UIScreen>();
    }

    public virtual void Init(Actor actor)
    {
        this.actor = actor;
        if (isRootScreen && subscreens != null)
        {
            foreach (var screen in subscreens)
            {
                if (screen != this && !screen.isRootScreen)
                    screen.Init(actor);
            }
        }
    }

    public virtual void Show()
    {
        if(!IsVisible)
        {
            UIManager.Instance?.Show(GetType());
        }
        ChangeVisibility(true);
    }
    
    public virtual void Hide()
    {
        if(IsVisible)
        {
            UIManager.Instance?.Hide(GetType());
        }
        ChangeVisibility(false);
    }
    
    public bool ShowHide()
    {
        return ChangeVisibility(!IsVisible);
    }

    public virtual bool ChangeVisibility(bool newState)
    {
        IsVisible = newState || alwaysActive;
        visualRoot.SetActive(IsVisible);
        if (IsVisible)
            PublishEvent(OnShow, EventArgs.Empty);
        else
            PublishEvent(OnHide, EventArgs.Empty);

        if (isRootScreen && subscreens != null)
            foreach (var screen in subscreens)
                if (screen != this && !screen.isRootScreen)
                    screen.ChangeVisibility(IsVisible);

        if(defaultSelection != null && newState)
            EventSystem.current.SetSelectedGameObject(defaultSelection);

        if (showCursorWhileActive)
        {
            if (IsVisible)
                Cursor.lockState = CursorLockMode.None;
            else
                Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = IsVisible;
        }

        return IsVisible;
    }

    protected virtual void PublishEvent(EventHandler handler, EventArgs e)
    {
        if (handler != null)
        {
            handler.Invoke(this, e);
        }
    }

    private void OnDestroy()
    {
        actor = null;
    }
}