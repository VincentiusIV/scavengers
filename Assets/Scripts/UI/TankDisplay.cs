﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TankDisplay : MonoBehaviour
{
    public TMP_Text textField;
    public TakeItemReaction takeItem;

    private void Update()
    {
        if (textField == null || takeItem == null)
            return;
        textField.text = string.Format("Fuel: {0}%", takeItem.Progress);
    }
}
