﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.InputSystem;


public static class UIUtil
{
    public static Vector2 CalculateCentre(RectTransform rectTransform)
    {
        Vector2 centre = new Vector2();
        centre.x = rectTransform.sizeDelta.x / 4f;
        centre.y = rectTransform.sizeDelta.y / 4f;
        return centre;
    }

    public static Vector2 FollowMouseOrCentre(RectTransform rectTransform, Vector2 centre)
    {
        Vector2 offset = CalculateCentre(rectTransform);
        Vector2 mousePosition = Input.mousePosition;

        return (mousePosition + offset);
    }
}
