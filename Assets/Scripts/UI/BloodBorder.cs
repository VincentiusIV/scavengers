﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodBorder : MonoBehaviour
{
    public Image bloodImage;
    public float interpolateSpeed = 10f;

    private Actor actor;

    private IEnumerator Start()
    {
        SetAlpha(bloodImage, 0);
        yield return new WaitUntil(() => UIManager.Instance.Actor != null);
        Track(UIManager.Instance.Actor);
    }

    public void Track(Actor actor)
    {
        this.actor = actor;
        SetAlpha(bloodImage, 1f - actor.NormHP);
    }

    private void Update()
    {
        if (actor == null) return;
        float currentAlpha = bloodImage.color.a;
        SetAlpha(bloodImage, Mathf.Lerp(currentAlpha, 1f - actor.NormHP, Time.deltaTime * interpolateSpeed));
    }

    private void SetAlpha(Image image, float alpha)
    {
        Color color = image.color;
        color.a = alpha;
        image.color = color;
    }
}
