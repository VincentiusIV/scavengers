﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Voice.Unity;
using Photon.Voice;
using TMPro;
using System;

public class VoiceInputChanger : MonoBehaviour
{
    public Recorder recorder;
    public TMP_Dropdown dropdown;

    private void Start()
    {
        var enumerator = Recorder.PhotonMicrophoneEnumerator;
        if (enumerator.IsSupported)
        {
            List<string> options = new List<string>();
            for (int i = 0; i < enumerator.Count; i++)
            {
                //Debug.LogFormat("PhotonMicrophone Index={0} ID={1} Name={2}", i, enumerator.IDAtIndex(i), enumerator.NameAtIndex(i));
                options.Add(enumerator.NameAtIndex(i));
            }
            dropdown.ClearOptions();
            dropdown.AddOptions(options);
        }

        dropdown.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnValueChanged(int arg0)
    {
        recorder.UnityMicrophoneDevice = dropdown.options[dropdown.value].text;
    }
}
