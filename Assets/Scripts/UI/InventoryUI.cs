﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;

public class InventoryUI : MonoBehaviour
{
    public ItemInspector itemInspector;
    public Transform slotAnchor;
    public SlotUI slotPrefab;
    private List<SlotUI> slotUIs;
    private InventoryComponent inventory;

    public void Show(InventoryComponent newInventory)
    {
        if (newInventory != null && newInventory != inventory)
        {
            inventory = newInventory;
            DrawSlots();
        }
    }

    public void DrawSlots()
    {
        slotAnchor.DestroyChildren();

        slotUIs = new List<SlotUI>();
        foreach (var slot in inventory.Slots)
        {
            SlotUI slotUI = Instantiate(slotPrefab, slotAnchor);
            slotUI.Setup(inventory, slot, itemInspector);
            slotUIs.Add(slotUI);
        }
    }
}
