﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorUI : UIScreen
{
    public GameObject restartGameButton;
    public BloodBorder bloodBorder;

    public override void Init(Actor actor)
    {
        base.Init(actor);

        actor.OnKilled.AddListener(OnKilled);
        actor.OnRespawn.AddListener(OnRespawn);

        restartGameButton.SetActive(actor.photonView.Owner.IsMasterClient);
    }

    private void OnRespawn()
    {
        UIManager.HideScreen(typeof(SpectatorUI));
    }

    private void OnKilled()
    {
        UIManager.ShowScreen(typeof(SpectatorUI));
    }

    public void OnSpectateNextButtonClicked()
    {
        SpectateCamera.Next();
        bloodBorder.Track(SpectateCamera.CurrentSpectatedActor);
    }

    public void OnSpectatePreviousButtonClicked()
    {
        SpectateCamera.Previous();
        bloodBorder.Track(SpectateCamera.CurrentSpectatedActor);
    }

    public void OnRestartGameButtonClicked()
    {
        SpaceshipManager.Instance.RestartGame();
    }
}
