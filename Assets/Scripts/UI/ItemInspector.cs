﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class ItemInspector : MonoBehaviour
{
    public GameObject root;
    public TMP_Text title;
    public TMP_Text weight;
    public TMP_Text description;
    public Image icon;
    public bool isVisible { get; private set; }
    private CanvasScaler scaler;
    private RectTransform rectTransform;
    private Vector3 stackPosition;

    private void Awake()
    {
        Debug.Assert(root != null);
        scaler = GetComponentInParent<CanvasScaler>();
        rectTransform = GetComponent<RectTransform>();
        Inspect(null, rectTransform.position);
    }

    private void Start()
    {
        Hide();
    }
    
    public void Inspect(Slot slot, Vector3 position)
    {
        if(slot == null || slot.IsEmpty)
            return;
        stackPosition = position;
        transform.position = position;
        Item item = slot.Contents.Item;
        title.text  = slot.Amount + " " + item.Name;
        if(weight)
            weight.text = item.Weight.ToString();
        description.text = item.Description;
        icon.sprite = item.Icon;
        Show();
    }

    private void Update()
    {
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        rectTransform.position = UIUtil.FollowMouseOrCentre(rectTransform, stackPosition);
    }

    public void Show()
    {
        UpdatePosition();
        isVisible = true;
        root.SetActive(true);
    }

    public void Hide()
    {
        isVisible = false;
        root.SetActive(false);
    }
}
