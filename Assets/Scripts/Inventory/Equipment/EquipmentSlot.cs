﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using Photon.Pun;

public class EquipmentSlot : Slot
{
    public ItemType slotType;
    public Equipable Equipable { get; private set; }
    private GameObject equipableObject { get; set; }
    private Actor owner;
    private EquipmentComponent equipment;
    
    public EquipmentSlot(InventoryComponent inventory, EquipmentComponent equip, ItemType slotType) : base(inventory)
    {
        this.slotType = slotType;
        this.equipment = equip;
        this.owner = equip.GetComponent<Actor>();
    }

    protected override void OnSlotFilled(SlotEventArgs e)
    {
        base.OnSlotFilled(e);
        equipment.EquippedItems.Add(Contents);
        Contents.State = ItemState.IN_USE;
        SpawnEquipable();
        EquipmentEventArgs equipEvent = new EquipmentEventArgs(owner, LogEventType.EQUIPPED, Contents.Item.AssetID, Contents);
        GameLog.DispatchEvent(equipment, equipment.Equipped, equipEvent);
    }

    private void SpawnEquipable()
    {
        GameObject itemPrefab = Contents.Item.Equipable;
        if (itemPrefab != null)
        {
            equipableObject = Object.Instantiate(itemPrefab, equipment.transform.position, equipment.transform.rotation);
            equipableObject.transform.localPosition = Vector3.zero;
            equipableObject.transform.localRotation = Quaternion.identity;
            Equipable = equipableObject.GetComponent<Equipable>();
            Equipable?.Setup(Contents, owner);
        }
    }

    protected override void OnSlotEmptied(SlotEventArgs e)
    {
        base.OnSlotEmptied(e);  
        RuntimeItem previousItem = e.previousItem;
        equipment.EquippedItems.Remove(previousItem);
        previousItem.State = ItemState.IDLE;
        Item equipmentItem = previousItem.Item as Item;
        DespawnEquipable();
        EquipmentEventArgs equipEvent = new EquipmentEventArgs(owner, LogEventType.UNEQUIPPED, previousItem.Item.AssetID, previousItem);
        GameLog.DispatchEvent(equipment, equipment.Unequipped, equipEvent);
    }

    private void DespawnEquipable()
    {
        if (Equipable != null)
        {
            Equipable.Deactivate();
            Equipable.Terminate(owner);
            Equipable = null;
        }
        if(equipableObject != null)
        {
            Object.Destroy(equipableObject);
            equipableObject = null;
        }
    }

    public override bool CanPlaceData(RuntimeItem runtimeItem)
    {
        if (runtimeItem == null) 
            return true;
        if (!runtimeItem.Item.IsEquipment)
            return false;
        Item equipmentItem = runtimeItem.Item;
        if (equipmentItem.itemType != slotType || equipment.IsItemEquipped(runtimeItem))
            return false;
        return base.CanPlaceData(runtimeItem);
    }
}
