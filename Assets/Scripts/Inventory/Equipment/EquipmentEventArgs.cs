﻿using System;
using System.Collections.Generic;

using UnityEngine;

public class EquipmentEventArgs : LogEvent
{
    public string ItemName { get; }
    public RuntimeItem RuntimeItem { get; }

    public EquipmentEventArgs(Actor actor, LogEventType eventType, string ItemName, RuntimeItem RuntimeItem) : base(actor, eventType)
    {
        this.ItemName = ItemName;
        this.RuntimeItem = RuntimeItem;
    }

    public override string Print()
    {
        return string.Format("{0} {1} {2}", SourceActor.name, EventType.ToString(), ItemName);
    }
}
