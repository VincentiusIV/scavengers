﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Bullet : MonoBehaviour
{
    public float speed = 100f, lifetime = 10f;
    public LayerMask hitMask;
    private Vector3 lastCheckPosition;
    private PhotonView photonView;
    private float timer;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        lastCheckPosition = transform.position;
    }

    private void Update()
    {
        if(photonView.IsMine)
        {
            transform.position += transform.forward * speed * Time.deltaTime;
            timer += Time.deltaTime;
            if (timer >= lifetime)
                PhotonNetwork.Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        if(photonView.IsMine)
        {
            RaycastHit hit;
            if (Physics.Raycast(lastCheckPosition, transform.forward, out hit, speed*Time.fixedDeltaTime, hitMask))
            {
                IHitable hitable = hit.collider.GetComponentInParent<IHitable>();
                hitable?.Hit();
                PhotonNetwork.Destroy(gameObject);
            }
            lastCheckPosition += transform.forward * speed * Time.fixedDeltaTime;
        }

    }
}
