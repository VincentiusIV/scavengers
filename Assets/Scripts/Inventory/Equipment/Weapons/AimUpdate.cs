﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AimUpdate : MonoBehaviour
{
    public Transform rotaty;
    public Actor targetActor;
    private Quaternion originalRotation;

    private void Awake()
    {
        originalRotation = rotaty.rotation;
    }

    private void Update()
    {
        rotaty.rotation = targetActor.IsAlive ? transform.rotation : originalRotation; 
    }

    private void OnDisable()
    {
        rotaty.rotation = originalRotation;
    }
}
