﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitable : MonoBehaviour
{
    public HitType hitType = HitType.DEFAULT;

    public void Hit(Vector3 hitPosition)
    {
        Hit(hitPosition, Quaternion.identity);
    }

    public virtual void Hit(Vector3 hitPosition, Quaternion rotation)
    {
        HitFXManager.PlayAtPosition(hitType, hitPosition, rotation);
    }
}
