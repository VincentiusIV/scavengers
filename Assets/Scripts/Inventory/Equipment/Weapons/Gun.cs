﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using System;
using TMPro;

public class Gun : Equipable
{
    public UnityEvent OnFire, OnReload, OnReloadFin, OnEmptyClip;
    public bool IsReloading => reloadTimer > 0f;
    public string blendTreeKey = "IsUsingGun";
    public float range = 1000f, TimeToReload = 3f;
    public int ClipSize = 4;
    public LayerMask hitMask;
    public Transform gunEnd;
    public Item bulletItem;
    public TMP_Text ammoText;
    public GameObject[] ammoBars;
    private int ammoInClip;
    private Transform playerCamera;
    private InventoryComponent inventory;
    private IKControl actorIKControl;
    private float reloadTimer;

    public override void Setup(RuntimeItem item, Actor actor)
    {
        base.Setup(item, actor);
        playerCamera = ((Scavengers)actor).playerCamera.transform;
        inventory = actor.GetComponent<InventoryComponent>();
        actorIKControl = actor.GetComponent<IKControl>();
    }

    public override void Activate()
    {
        if (IsActive) return;
        base.Activate();
        Debug.Log("Activating equipable " + gameObject.name, gameObject);
        actor.animator.SetBool(blendTreeKey, true);
        actorIKControl.SetAimRig(true);
        UpdateClipBars();
    }

    public override void Deactivate()
    {
        if (!IsActive) return;
        base.Deactivate();
        Debug.Log("Deactivating equipable " + gameObject.name, gameObject);
        actor.animator.SetBool(blendTreeKey, false);
        actorIKControl.SetAimRig(false);
    }

    private void Update()
    {
        if(IsReloading)
        {
            reloadTimer -= Time.deltaTime;
            reloadTimer = Mathf.Max(0, reloadTimer);
            if(!IsReloading)
            {
                OnReloadFin.Invoke();
                actor.animator.SetBool(blendTreeKey, true);
                actorIKControl.SetAimRig(true);
                UpdateClipBars();
            }
        }
    }

    public override void ExecuteCommand0()
    {
        base.ExecuteCommand0();
        if(ammoInClip == 0)
        {
            Reload();
            return;
        }

        if (!IsReloading && ammoInClip > 0)
        {
            --ammoInClip;
            UpdateClipBars();
            OnFire.Invoke();
            
            if(actor.photonView.IsMine)
            {
                Vector3 fwd = playerCamera.position + playerCamera.forward * range;
                Vector3 fromGunEnd = (fwd - gunEnd.position).normalized;
                PhotonNetwork.Instantiate(bulletItem.equipable.name, gunEnd.position, Quaternion.LookRotation(fromGunEnd, Vector3.up));
            }
        }
        else
        {
            OnEmptyClip.Invoke();
        }
    }

    private void Reload()
    {
        int numInInv = inventory.GetTotalAmountOfItem(bulletItem.AssetID);
        if (numInInv == 0)
            return;
        int numRemoved = Mathf.Min(ClipSize, numInInv);
        inventory.RemoveItemByID(bulletItem.AssetID, numRemoved);
        ammoInClip = numRemoved;

        reloadTimer = TimeToReload;
        OnReload.Invoke();
        actor.animator.SetBool(blendTreeKey, false);
        actorIKControl.SetAimRig(false);
    }

    private void UpdateClipBars()
    {
        if(ammoText != null)
            ammoText.text = ammoInClip.ToString();
        for (int i = 0; i < ammoBars.Length; i++)
        {
            if(ammoBars[i] != null)
                ammoBars[i].SetActive((i < ammoInClip));
        }
    }
}
