﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using System;
using UnityEngine.Rendering.HighDefinition;

public class Flashlight : Equipable
{
    public UnityEvent OnActivate, OnDeactivate, OnNoBattery, OnReload;
    public Light activeFlashlight, passiveLight, activeVolumetricFlashlight, passiveVolumetricFlashlight;
    private bool useVolumetrics;

    public Item batteryRequired;
    public float timePerBattery = 10f;
    public float timeBetweenReload = 1f;
    private float timeRemaining;
    private InventoryComponent playerInventory;
    public TriggerEventListener litTrigger;
    private HashSet<IShinable> inTrigger;
    private List<IShinable> lit;

    protected override void Awake()
    {
        base.Awake();
        inTrigger = new HashSet<IShinable>();
        lit = new List<IShinable>();
    }

    private void OnProximityEnter(object sender, TriggerEventArgs e)
    {
        IShinable shinable = e.Collider.GetComponentInParent<IShinable>();
        if(shinable != null && !inTrigger.Contains(shinable))
        {
            inTrigger.Add(shinable);
        }
    }

    private void OnProximityExit(object sender, TriggerEventArgs e)
    {
        IShinable shinable = e.Collider.GetComponentInParent<IShinable>();
        if (shinable != null && inTrigger.Contains(shinable))
        {
            inTrigger.Remove(shinable);
            RemoveLit(shinable);
        }
    }

    public override void Setup(RuntimeItem item, Actor actor)
    {
        this.item = item;
        this.actor = actor;
        playerInventory = actor.GetComponent<InventoryComponent>();
        Reload();
        if(actor.photonView.IsMine)
        {
            litTrigger.ProximityEnter += OnProximityEnter;
            litTrigger.ProximityExit += OnProximityExit;
        }
    }

    protected override void Start()
    {
        base.Start();
        Deactivate();
    }

    private void Update()
    {
        if (!actor.photonView.IsMine)
            return;

        if(IsActive)
        {
            if(timeRemaining > 0f)
                foreach (var item in inTrigger)
                    AddLit(item);

            timeRemaining -= Time.deltaTime;
        }
        else if(!IsActive)
        {
            while(lit.Count > 0)
                RemoveLit(lit[0]);
        }
    }

    public bool ShouldDeactivate()
    {
        return IsActive && timeRemaining <= 0f;
    }

    private void AddLit(IShinable shinable)
    {
        if(!lit.Contains(shinable))
        {
            lit.Add(shinable);
            shinable.Lit();
        }
    }

    private void RemoveLit(IShinable shinable)
    {
        if(lit.Contains(shinable))
        {
            lit.Remove(shinable);
            shinable.Unlit();
        }
    }

    public void Switch()
    {
        if (IsActive)
            Deactivate();
        else
            Activate();
    }

    public void ReloadIfPlayerHasBattery()
    {
        bool hasBattery = playerInventory.ContainsItem(batteryRequired.AssetID);
        Debug.LogWarning(hasBattery ? "Player has a battery" : "YOU NO HAVE BATTERIES");
        if (hasBattery)
        {
            Reload();
            playerInventory.RemoveItemByID(batteryRequired.AssetID, 1);
            Debug.Log("Flashlight: Reloaded, remove 1 battery from inventory");
        }
        else
        {
            OnNoBattery.Invoke();
        }
    }

    public void Reload()
    {
        timeRemaining = timePerBattery;
        OnReload.Invoke();
    }

    public override void Activate()
    {
        if(timeRemaining <= 0f && actor.photonView.IsMine)
            ReloadIfPlayerHasBattery();

        IsActive = true;
        UpdateLights();
        OnActivate.Invoke();
    }

    public override void Deactivate()
    {
        IsActive = false;
        UpdateLights();
        OnDeactivate.Invoke();
    }

    public void EnableVolumetricLights()
    {
        useVolumetrics = true;
        UpdateLights();
    }

    public void DisableVolumetricLights()
    {
        useVolumetrics = false;
        UpdateLights();
    }

    private void UpdateLights()
    {
        activeFlashlight.enabled = IsActive && !useVolumetrics;
        passiveLight.enabled = !IsActive && !useVolumetrics;
        activeVolumetricFlashlight.enabled = IsActive && useVolumetrics;
        passiveVolumetricFlashlight.enabled = !IsActive && useVolumetrics;
    }
}
