﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct HoldsterOverride
{
    public HoldsterType holdsterType;
    public Vector3 positionOffset;
    public Vector3 eulerAngle;
}
