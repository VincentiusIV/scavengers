﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public abstract class Equipable : MonoBehaviour
{
    public bool IsActive { get; protected set; }
    public Actor actor { get; protected set; }
    protected new Rigidbody rigidbody;
    protected RuntimeItem item;

    protected virtual void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    protected virtual void Start()
    {
    }

    public virtual void Setup(RuntimeItem item, Actor actor)
    {
        this.item = item;
        this.actor = actor;

        EquipmentComponent equip = actor.GetComponent<EquipmentComponent>();
        transform.SetParent(equip.rightHandTf);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }

    public virtual void Activate()
    {
        if (IsActive) return;
        IsActive = true;
        gameObject.SetActive(true);
    }

    public virtual void Deactivate()
    {
        if (!IsActive) return;
        IsActive = false;
        gameObject.SetActive(false);
    }

    public virtual void ExecuteCommand0()
    {

    }

    public virtual void ExecuteCommand1()
    {

    }

    public virtual void Terminate(Actor owner)
    {

    }
}
