﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

/// <summary>
/// A hotbar manages a set of slots.
/// </summary>
public class Hotbar
{
    public EventHandler OnSwap;
    public EquipmentSlot[] Slots { get; }
    public EquipmentSlot CurrentSlot { get { if (currentSlotIdx >= 0 && currentSlotIdx < Slots.Length) return Slots[currentSlotIdx]; else return null; } }
    public Equipable CurrentEquipable { get { return CurrentSlot?.Equipable; } }  
    public int NumOfOptions {
        get
        {
            int count = 0;
            foreach (var slot in Slots) {
                if (!slot.IsEmpty)
                    ++count;
            }
            return count;
        }
    }
    protected int currentSlotIdx = -1;
    protected Actor owner;

    public Hotbar(Actor owner, EquipmentSlot[] slots)
    {
        this.owner = owner ?? throw new ArgumentNullException(nameof(owner));
        this.owner.OnLockChanged.AddListener(OnLockChanged);
        Slots = slots ?? throw new ArgumentNullException(nameof(slots));
        
        foreach (var slot in Slots) {
            slot.Changed += OnSlotChanged;
        }
    }

    private void OnLockChanged()
    {

    }

    ~Hotbar()
    {
        foreach (var slot in Slots)
            slot.Changed -= OnSlotChanged;
        this.owner.OnLockChanged.RemoveListener(OnLockChanged);
    }

    protected virtual void OnSlotChanged(object sender, SlotEventArgs e)
    {
        
    }

    public void SetCurrent(Equipable equipable, bool unsetIfCurrent = true)
    {
        if (!unsetIfCurrent && IsCurrent(equipable))
            return;

        int indexOfEquipable = -1;
        for (int i = 0; i < Slots.Length; i++)
        {
            if (Slots[i].Equipable == equipable)
            {
                indexOfEquipable = i;
                break;
            }
        }

        SetCurrent(indexOfEquipable);
    }

    public void SetCurrent(int index)
    {
        if (index < 0 || index >= Slots.Length)
            return;

        if (CurrentEquipable != null && CurrentEquipable.IsActive)
        {
            CurrentEquipable.Deactivate();
        }
        else if (CurrentEquipable != null && !CurrentEquipable.IsActive)
        {
            CurrentEquipable.Activate();
        }

        if (currentSlotIdx != index)
        {
            currentSlotIdx = index;
            CurrentEquipable?.Activate();
        }
        else
        {
            currentSlotIdx = -1;
        }

        if (OnSwap != null)
            OnSwap.Invoke(this, EventArgs.Empty);
    }
    
    public void SetAnyAsCurrent()
    {
        if (CurrentEquipable != null)
        {
            if (!CurrentEquipable.IsActive)
                SetCurrent(CurrentEquipable);
            return;
        }

        foreach (var slot in Slots)
        {
            if (slot.IsEmpty || slot.Equipable == null)
                continue;

            if(!slot.Equipable.IsActive)
            {
                SetCurrent(slot.Equipable);
                break;
            }
        }
    }

    public void Next() {
        for (int i = 1; i < Slots.Length; i++)
        {
            int idx = (currentSlotIdx + i) % Slots.Length;
            if(!Slots[idx].IsEmpty)
            {
                SetCurrent(idx);
                break;
            }
        }
    }

    public void Previous()
    {
        for (int i = 1; i < Slots.Length; i++)
        {
            int idx = (currentSlotIdx - i);
            if (idx < 0) idx += Slots.Length;
            if (!Slots[idx].IsEmpty)
            {
                SetCurrent(idx);
                break;
            }
        }
    }

    public bool IsCurrent(Equipable equipable)
    {
        return CurrentEquipable == equipable;
    }

    public bool HasEquipped(Item item)
    {
        foreach (var slot in Slots)
        {
            if (slot.Contents?.Item == item)
                return true;
        }
        return false;
    }
}
