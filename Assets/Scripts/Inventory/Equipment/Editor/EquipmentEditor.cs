﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EquipmentComponent))]
public class EquipmentEditor : Editor
{
    EquipmentComponent component;

    private GUIStyle titleStyle;

    private void OnEnable()
    {
        component = target as EquipmentComponent;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (titleStyle == null)
        {
            titleStyle = new GUIStyle();
            titleStyle.fontStyle = FontStyle.BoldAndItalic;
        }

    }

}
