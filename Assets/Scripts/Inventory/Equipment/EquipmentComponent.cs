using System;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using Photon.Pun;

public enum HoldsterType
{
    DEFAULT,
    LEFT_HAND,
    RIGHT_HAND,
    BELT_LEFT,
    BELT_RIGHT,
    BACK_1,
    BACK_2,
}

[System.Serializable]
public struct HoldsterPair
{
    public HoldsterType type;
    public Transform worldReference;
}

[Serializable]
public class EquipmentDefault
{
    public ItemType equipmentType;
    public GameObject worldReference;
}

public class EquipmentComponent : MonoBehaviour
{
	public EventHandler<EquipmentEventArgs> Equipped;
	public EventHandler<EquipmentEventArgs> Unequipped;
    public Transform rightHandTf;
	private InventoryComponent inventory;
    public List<RuntimeItem> EquippedItems { get; private set; }
    public Hotbar Hotbar { get; set; }
    public EquipmentSlot[] slots;
    protected Actor owner;

    private void Awake()
    {
        owner = GetComponent<Actor>();
        owner.OnKilled.AddListener(OnKilled);
        inventory = GetComponent<InventoryComponent>();
        inventory.ItemAdded += OnItemAdded;
        inventory.ItemRemoved += OnItemRemoved;
        EquippedItems = new List<RuntimeItem>();
        slots = new EquipmentSlot[inventory.capacity];
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i] = new EquipmentSlot(inventory, this, ItemType.WEAPON);
        }
        Hotbar = new Hotbar(owner, slots);
    }

    private void OnKilled()
    {
        foreach (var s in slots)
        {
            UnequipItem(s.Contents);
        }
    }

    private void Update()
    {
        if(owner.photonView.IsMine && Input.GetKeyDown(KeyCode.F1))
        {
            inventory.AddItemByID("BATTERY");
        }
        if (owner.photonView.IsMine && Input.GetKeyDown(KeyCode.F2))
        {
            inventory.AddItemByID("FUEL");
        }
        if (owner.photonView.IsMine && Input.GetKeyDown(KeyCode.F3))
        {
            inventory.AddItemByID("BLASTERAMMO");
        }

        if (owner.photonView.IsMine && Input.GetMouseButtonDown(0) && owner.CanControl)
        {
            owner.photonView.RPC("ExecuteCommand0Rpc", RpcTarget.All);
        }
        if (owner.photonView.IsMine && Input.GetMouseButtonDown(1) && owner.CanControl)
        {
            owner.photonView.RPC("ExecuteCommand1Rpc", RpcTarget.All);
        }
    }

    [PunRPC]
    private void ExecuteCommand0Rpc()
    {
        Hotbar.CurrentEquipable?.ExecuteCommand0();
    }

    [PunRPC]
    private void ExecuteCommand1Rpc()
    {
        Hotbar.CurrentEquipable?.ExecuteCommand1();
    }

    private void OnItemAdded(object sender, InventoryEventArgs e)
    {
        if(e.RuntimeItem.Item.itemType == ItemType.WEAPON)
        {
            EquipItem(e.RuntimeItem);
            Hotbar.SetAnyAsCurrent();
        }
    }

    private void OnItemRemoved(object sender, InventoryEventArgs e)
    {
        if(e.RuntimeItem.Item.itemType == ItemType.WEAPON)
        {
            UnequipItem(e.RuntimeItem);
            Hotbar.SetAnyAsCurrent();
        }
    }

    public bool EquipItem(RuntimeItem runtimeItem)
    {
        if (runtimeItem == null || !runtimeItem.Item.IsEquipment || IsItemEquipped(runtimeItem))
            return false;
        EquipmentSlot firstFree = GetFirstFreeSlot();
        if (firstFree == null)
            return false;
        return firstFree.SetData(runtimeItem, 1);
    }

    public bool UnequipItem(RuntimeItem runtimeItem)
    {
        EquipmentSlot slot = FindSlotWithItem(runtimeItem);
        if (Hotbar.CurrentSlot == slot)
            Hotbar.Next();
        if (slot == null)
            return false;
        return slot.SetData(null, 0);
    }

    private EquipmentSlot FindSlotWithItem(RuntimeItem item)
    {
        foreach (var slot in slots)
            if (slot.Contents == item)
                return slot;
        return null;
    }

    public EquipmentSlot GetFirstFreeSlot()
    {
        foreach (var slot in slots)
            if (slot.IsEmpty)
                return slot;
        return null;
    }

    public bool IsItemEquipped(RuntimeItem runtimeItem)
    {
        foreach (var slot in slots)
            if (slot.Contents == runtimeItem)
                return true;
        return false;
    }

    private void OnDestroy()
    {
        foreach (var slot in slots)
        {
            if(!slot.IsEmpty)
                UnequipItem(slot.Contents);
        }
    }
}