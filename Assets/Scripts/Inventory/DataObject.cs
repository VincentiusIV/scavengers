﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPGData
{
    public class DataObject : ScriptableObject
    {
        public string AssetID { get => id; private set => id = value; }
        public virtual string AssetName
        {
            get
            {
                return assetName;
            }
            set
            {
                assetName = value;
                UpdateID();
            }
        }

        public int GroupNameIdx { get => groupNameIdx; set => groupNameIdx = value; }
        [SerializeField] private string id;
        [HideInInspector] public string assetName;
        [HideInInspector] public int groupNameIdx;

        public bool Equals(DataObject other)
        {
            return other != null && AssetID == other.AssetID;
        }

        public virtual void Init()
        {
            AssetName = AutoCreateName();
        }

        protected void UpdateID()
        {
            if (assetName == null)
                assetName = AutoCreateName();
            AssetID = assetName.Trim();
            AssetID = AssetID.Replace(' ', '_');
            AssetID = AssetID.Replace("__", "_");
            AssetID = AssetID.ToUpper();
        }


        public virtual string AutoCreateName()
        {
            return "New Data Object " + GetInstanceID();
        }

        public virtual bool CanBeDeletedSafely()
        {
            return true;
        }

        public virtual bool MatchesFilter(string filter)
        {
            if (filter.Length > 0)
            {
                if (AssetID.Contains(filter) || AssetName.Contains(filter))
                    return true;
                else
                    return false;
            }
            return true;
        }


        public  virtual void UpdateDependencies()
        {
            // Do nothing
        }
    }

    public interface IConsumable
    {
        List<DataObject> Consumers { get; }
        void AddConsumer(DataObject consumer);
        void Clear();
    }

    /// <summary>
    /// DataObject that keeps track of other DataObjects that depend on this one.
    /// </summary>
    public class ConsumableDataObject : DataObject, IConsumable
    {
        public List<DataObject> Consumers => consumers;
        [SerializeField] private List<DataObject> consumers = new List<DataObject>();

        public void AddConsumer(DataObject consumer)
        {
            if (consumers == null)
                consumers = new List<DataObject>();
            if (!consumers.Contains(consumer))
                consumers.Add(consumer);
        }

        public virtual void Clear()
        {
            consumers?.Clear();
        }
#if UNITY_EDITOR
        public override bool CanBeDeletedSafely()
        {
            return consumers.Count == 0;
        }
#endif
    }

    public interface IProducable
    {
        List<DataObject> Producers { get; }
        void AddProducer(DataObject producer);
        void Clear();
    }

    /// <summary>
    /// ConsumableDataObject that keeps track of other DataObjects that create this one.
    /// </summary>
    public class ProducableDataObject : ConsumableDataObject, IProducable
    {
        public List<DataObject> Producers => producers;
        [SerializeField] private List<DataObject> producers = new List<DataObject>();

        public void AddProducer(DataObject producer)
        {
            if (!producers.Contains(producer))
                producers.Add(producer);
        }

        public override void Clear()
        {
            base.Clear();
            producers.Clear();
        }        
    }
}

