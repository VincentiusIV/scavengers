﻿using RPGData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IData
{
    string AssetID { get; set; }
    string AssetName { get; }
}

public interface IDataCollection
{
    void Init();
    DataObject[] Items { get; }
}