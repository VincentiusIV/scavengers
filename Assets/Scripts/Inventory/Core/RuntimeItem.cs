﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemState
{
    NEW,
    IDLE, 
    IN_USE,
}

/// <summary>
/// Holds a reference to an item and its state at runtime.
/// </summary>
public class RuntimeItem
{
    public Item Item { get; }
    public ItemState State { get; set;}

    // TODO: Upgrades?

    public RuntimeItem(Item item)
    {
        Item = item;
    }
}
