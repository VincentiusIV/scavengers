﻿using RPGData;
using System;
using System.Collections.Generic;

using UnityEngine;

public static class AssetDatabaseExtensions
{
    public static TData[] SortAlphabetically<TData>(TData[] dataObjects)
        where TData : DataObject
    {
        List<TData> sorted = new List<TData>();
        for (int i = 0; i < dataObjects.Length; i++)
        {
            TData currentAsset = dataObjects[i];

            int sortedIndex = 0;
            for (int j = 0; j < sorted.Count; j++)
            {
                TData sortedAsset = sorted[j];
                bool isBetter = false, undecided = false;
                int minLength = Mathf.Min(currentAsset.AssetID.Length, sortedAsset.AssetID.Length);

                for (int z = 0; z < minLength; z++)
                {
                    char sortedAssetChar = sortedAsset.AssetID[z];
                    char currentAssetChar = currentAsset.AssetID[z];
                    if (sortedAssetChar == currentAssetChar)
                        continue;
                    if (currentAssetChar > sortedAssetChar)
                    {
                        break;
                    }
                    if (currentAssetChar < sortedAssetChar)
                    {
                        isBetter = true;
                        break;
                    }
                    undecided = (z == (minLength - 1));
                }
                if (undecided)
                {
                    isBetter = (currentAsset.AssetID.Length == minLength);
                }
                if (isBetter)
                    break;
                else
                    ++sortedIndex;
            }

            sorted.Insert(sortedIndex, currentAsset);
        }
        return sorted.ToArray();
    }



}
