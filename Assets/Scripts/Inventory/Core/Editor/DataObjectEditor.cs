﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using RPGData;

public class DataObjectEditor : Editor
{
    public int Index { get; set; }
    public bool IsExpanded { get; set; } = false;
    protected DataObject dataObject;
    private IDataCollectionEditorOperations dataCollectionOperations;
    private int newIndex = -1;

    protected virtual void OnEnable()
    {
        this.dataObject = target as DataObject;
    }

    public void Setup(DataObject dataObject, IDataCollectionEditorOperations dataCollectionOperations)
    {
        this.dataObject = dataObject;
        this.dataCollectionOperations = dataCollectionOperations;
    }

    public void ResetIndex()
    {
        newIndex = Index;
    }

    public virtual string GetAssetName()
    {
        return (dataObject.AssetName != null && dataObject.AssetName.Length > 0) ? dataObject.AssetName : dataObject.AssetID;
    }

    public virtual void OnRPGEditorGUI(bool isInspected)
    {
        if (dataObject == null)
            return;
        if (newIndex == -1)
            newIndex = Index;
        EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(false));
        EditorGUILayout.BeginHorizontal();
        DrawMoveControls();
        if (newIndex != Index && GUILayout.Button("Move", GUILayout.Width(50)))
            dataCollectionOperations.Move(dataObject, newIndex);
        EditorGUILayout.BeginHorizontal();
        string dirtyAdd = EditorUtility.IsDirty(dataObject) ? "*" : "";
        //EditorGUILayout.LabelField(GetAssetName() + dirtyAdd, GUILayout.Width(200));
        TextAnchor textAnchor = GUI.skin.button.alignment;
        GUI.skin.button.alignment = isInspected ? TextAnchor.MiddleRight : TextAnchor.MiddleLeft;
        if (GUILayout.Button(GetAssetName() + dirtyAdd))
            dataCollectionOperations.Inspect(dataObject);
        GUI.skin.button.alignment = textAnchor;
        EditorGUILayout.EndHorizontal();
        //IsExpanded = EditorGUILayout.Foldout(IsExpanded, "Details", true);
        //DrawQuickAccessButtons();

        EditorGUILayout.EndHorizontal();
        if (IsExpanded)
        {
            ++EditorGUI.indentLevel;
            DetailPanel();
            --EditorGUI.indentLevel;
        }
        EditorGUILayout.EndVertical();
    }

    private void DrawMoveControls()
    {
        //if (GUILayout.Button("^", GUILayout.Width(20)))
        //    dataCollectionOperations.MoveUp(dataObject);
        newIndex = EditorGUILayout.IntField(newIndex, GUILayout.Width(40));
        //if (GUILayout.Button("v", GUILayout.Width(20)))
        //    dataCollectionOperations.MoveDown(dataObject);
    }

    public override void OnInspectorGUI()
    {
        DrawQuickAccessButtons();
        base.OnInspectorGUI();

    }

    public virtual void DetailPanel()
    {
        if (dataObject == null)
            return;
        DrawQuickAccessButtons();
        DrawDefaultInspector();
    }

    protected virtual void DrawQuickAccessButtons()
    {
        if (dataCollectionOperations == null)
            return;

        EditorGUILayout.BeginHorizontal();
        DrawAdditiveUtilityButtons();
        int grpIdx = dataCollectionOperations.GroupField(dataObject.GroupNameIdx);
        if(grpIdx != dataObject.GroupNameIdx)
        {
            dataObject.GroupNameIdx = grpIdx;
            EditorUtility.SetDirty(dataObject);
        }
        if (GUILayout.Button("~", GUILayout.Width(20)))
        {
            dataObject.AssetName = dataObject.AutoCreateName();
        }
        if (GUILayout.Button("$", GUILayout.Width(20)))
        {
            Selection.SetActiveObjectWithContext(dataObject, this);
        }
        if (GUILayout.Button("+", GUILayout.Width(20)))
        {
            dataCollectionOperations.Duplicate(dataObject);
        }
        if (GUILayout.Button("-", GUILayout.Width(20)))
        {
            dataCollectionOperations.Remove(dataObject);
        }
        Color bgcolor = GUI.backgroundColor;
        GUI.backgroundColor = Color.red;
        if (GUILayout.Button("--", GUILayout.Width(20)))
        {
            dataCollectionOperations.TryDelete(dataObject);
        }
        GUI.backgroundColor = bgcolor;



        EditorGUILayout.EndHorizontal();
    }

    protected virtual void DrawAdditiveUtilityButtons()
    {

    }
}
