﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public static class EditorExtensions
{
    public static void DropAreaGUI(string label, float height, Action<UnityEngine.Object> dropCallback)
    {
        Event evt = Event.current;
        Rect dropArea = GUILayoutUtility.GetRect(0.0f, height, GUILayout.ExpandWidth(true));
        GUI.Box(dropArea, label);

        switch (evt.type)
        {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!dropArea.Contains(evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform)
                {
                    DragAndDrop.AcceptDrag();

                    foreach (UnityEngine.Object draggedObject in DragAndDrop.objectReferences)
                    {
                        dropCallback.Invoke(draggedObject);
                    }
                }
                break;
        }
    }
}
