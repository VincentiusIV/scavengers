﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using RPGData;

public interface IDataCollectionEditorOperations
{
    void Duplicate(DataObject dataObject);
    void TryDelete(DataObject dataObject);
    void Remove(DataObject dataObject);
    void MoveUp(DataObject dataObject);
    void MoveDown(DataObject dataObject);
    void Move(DataObject dataObject, int index);
    void Inspect(DataObject dataObject);
    int GroupField(int groupIndex);
}

public class DataCollectionEditor<TData, TEditor> : DataObjectEditor, IDataCollectionEditorOperations
    where TData : DataObject
    where TEditor : DataObjectEditor
{
    protected DataCollection<TData> dataCollection;
    protected Dictionary<TData, TEditor> editors;
    protected Vector2 scrollPosition;
    protected string nameFilter = "";
    protected string groupNames;
    protected string[] typeNames;
    protected Type[] types;
    protected int selectedTypeIdx = 0;
    protected int currentGroupNameIdx;
    protected bool editResourcePath;
    int drawingIndexCounter = 0;
    protected DataObjectEditor inspector;
    protected DataObject inspectedObject;

    protected override void OnEnable()
    {
        dataCollection = target as DataCollection<TData>;
        
        editors = new Dictionary<TData, TEditor>();
        groupNames = "";
        if (dataCollection.groupNames == null)
            dataCollection.groupNames = new string[0];
        foreach (var gp in dataCollection.groupNames)
        {
            groupNames += gp + ", ";
        }
        types = Utility.FetchTypes<TData>(out typeNames, true);
    }

    public override void OnInspectorGUI()
    {
        if (dataCollection == null)
        {
            dataCollection = target as DataCollection<TData>;
            return;
        }
        if(dataCollection.Items == null)
        {
            dataCollection.Items = new TData[0];
        }

        EditorExtensions.DropAreaGUI("Add Area", 50f, delegate(UnityEngine.Object obj) { dataCollection.Add(obj as TData); });
        DrawUtilityButtons();
        DrawDataCollection();
    }

    protected void DrawUtilityButtons()
    {
        EditorGUILayout.BeginHorizontal();
        HeaderUtilityButtons();
        EditorGUILayout.EndHorizontal();

        if (editResourcePath)
            dataCollection.resourcePath = EditorGUILayout.TextField("Resource Path", dataCollection.resourcePath);
        else
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Resource Path: " + dataCollection.resourcePath, GUILayout.Width(300));
            editResourcePath = GUILayout.Button("Edit", GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();
        }
        nameFilter = EditorGUILayout.TextField("Name/ID Filter", nameFilter).ToUpper();
        GroupNameEditing();
    }

    protected virtual void HeaderUtilityButtons()
    {
        if (GUILayout.Button("Save", GUILayout.Width(150)))
        {
            Save();
        }
        if (GUILayout.Button("Sort", GUILayout.Width(150)))
        {
            SortAlphabetically();
        }
    }

    private void SortAlphabetically()
    {
        dataCollection.Items = AssetDatabaseExtensions.SortAlphabetically(dataCollection.Items);
        ResetEditorIndices();
        EditorUtility.SetDirty(dataCollection);
        AssetDatabase.SaveAssets();
    }

    private void ResetEditorIndices()
    {
        int resetIndexCounter = 0;
        foreach (var item in dataCollection.Items)
        {
            editors[item].Index = resetIndexCounter++;
        }
        foreach (var editorPair in editors)
        {
            editorPair.Value.ResetIndex();
        }
    }

    private void Save()
    {
        editResourcePath = false;
        for (int i = 0; i < dataCollection.Items.Length; i++)
        {
            if (dataCollection.Items[i] == null)
            {
                TData[] temp = new TData[dataCollection.Items.Length - 1];
                Array.Copy(dataCollection.Items, temp, i);
                Array.Copy(dataCollection.Items, i + 1, temp, i, dataCollection.Items.Length - i - 1);
                dataCollection.Items = temp;
            }
        }

        RenameAssetsToIDs();
        AssetDatabase.SaveAssets();
    }

    private void RenameAssetsToIDs()
    {
        foreach (var item in dataCollection.Items)
        {
            if (item.AssetID != item.name)
            {
                string oldPath = AssetDatabase.GetAssetPath(item);
                string newPath = dataCollection.CreateUniquePath(item);
                if (AssetDatabase.LoadAssetAtPath(newPath, typeof(UnityEngine.Object)) != null)
                {
                    Debug.LogError("Could not rename asset since another exists of the same name!", item);
                    continue;
                }
                AssetDatabase.RenameAsset(oldPath, item.AssetID);
            }
        }
        EditorUtility.SetDirty(dataCollection);
    }

    private void GroupNameEditing()
    {
        EditorGUILayout.BeginHorizontal();
        groupNames = EditorGUILayout.TextField("Groups", groupNames);
        if (GUILayout.Button("Update Groups", GUILayout.Width(100)))
        {
            List<string> groupNameList = new List<string>();
            groupNameList.AddRange(groupNames.Trim().Split(','));
            for (int i = 0; i < groupNameList.Count; i++)
            {
                groupNameList[i] = groupNameList[i].Trim();
                if (groupNameList[i].Length == 0)
                    groupNameList.RemoveAt(i--);
            }
            dataCollection.groupNames = groupNameList.ToArray();
            EditorUtility.SetDirty(dataCollection);
        }

        currentGroupNameIdx = EditorGUILayout.Popup("Show group", currentGroupNameIdx, dataCollection.groupNames);
        EditorGUILayout.EndHorizontal();
    }

    protected void DrawDataCollection()
    {
        EditorGUILayout.BeginHorizontal();
        scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUILayout.Width(300));
        drawingIndexCounter = -1;
        int visibleItemCounter = 0;
        EditorGUILayout.BeginVertical();
        foreach (var item in dataCollection.Items)
        {
            ++drawingIndexCounter;
            if (item == null)
                continue;
            if (!item.MatchesFilter(nameFilter.Trim()))
                continue;
            if (currentGroupNameIdx > 0 && currentGroupNameIdx != item.GroupNameIdx)
                continue;

            ++visibleItemCounter;
            //EditorGUILayout.Space();
            Rect layoutRect = EditorGUILayout.BeginVertical();

            var editor = CreateEditorIfNotExist(item);
            Color backgroundColor = EditorGUIUtility.isProSkin
                ? new Color32(40, 40, 40, 255)
                : new Color32(194, 194, 194, 255);
            if (editor.IsExpanded)
                backgroundColor -= new Color32(10, 10, 10, 0);
            EditorGUI.DrawRect(layoutRect, backgroundColor);
            DrawDataObjectEditor(item, layoutRect);
            EditorGUILayout.EndVertical();
        }

        DrawCreateTool();

        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();

        if (inspector == null && dataCollection.Items.Length > 0)
            Inspect(dataCollection.Items[0]);

        if(inspector && inspectedObject)
        {
            Rect layoutRect = EditorGUILayout.BeginVertical();
            Color backgroundColor = EditorGUIUtility.isProSkin
                ? new Color32(40, 40, 40, 255)
                : new Color32(194, 194, 194, 255);
            EditorGUI.DrawRect(layoutRect, backgroundColor);
            EditorGUILayout.LabelField("Inspector - " + inspectedObject.AssetName);
            inspector.DetailPanel();
            EditorGUILayout.EndVertical();
        }

        EditorGUILayout.EndHorizontal();
    }

    private void DrawCreateTool()
    {
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Create New", GUILayout.Width(150)))
        {
            TryCreateNewAsset();
        }
        if (types.Length > 1)
        {
            selectedTypeIdx = EditorGUILayout.Popup(selectedTypeIdx, typeNames);
        }
        EditorGUILayout.EndHorizontal();
    }

    protected virtual TData TryCreateNewAsset()
    {
        return dataCollection.Create(types[selectedTypeIdx]);
    }

    public void Inspect(DataObject dataObject)
    {
        inspector = CreateEditor(dataObject) as DataObjectEditor;
        inspector.Setup(dataObject, this);
        inspectedObject = dataObject;
    }

    protected virtual void DrawDataObjectEditor(TData dataObject, Rect backgroundRect)
    {
        CreateEditorIfNotExist(dataObject);
        editors[dataObject].Index = drawingIndexCounter;
        editors[dataObject].OnRPGEditorGUI(inspectedObject == dataObject);
    }

    private TEditor CreateEditorIfNotExist(TData dataObject)
    {
        if (!editors.ContainsKey(dataObject))
        {
            var editor = CreateEditor(dataObject) as TEditor;
            editor.Setup(dataObject, this);
            editors.Add(dataObject, editor);
        }
        return editors[dataObject];
    }

    public void Duplicate(DataObject dataObject)
    {
        TData asset = (TData)Instantiate(dataObject);
        asset.Init();
        //asset.AssetName += asset.GetInstanceID().ToString();
        string path = dataCollection.CreateUniquePath(asset);
        dataCollection.Add(asset);
        AssetDatabase.CreateAsset(asset, path);
        AssetDatabase.SaveAssets();
    }

    public void TryDelete(DataObject dataObject)
    {
        if (dataObject.CanBeDeletedSafely())
        {
            Delete(dataObject);
        }
        else
        {
            if (EditorUtility.DisplayDialog("This data object cannot be deleted safely.", "It is likely that other data depends on this object, are you sure you want to delete it?", "Yes", "No"))
            {
                Delete(dataObject);
            }
        }
    }

    private void Delete(DataObject dataObject)
    {
        if (EditorUtility.DisplayDialog("Delete Confirmation", "Are you sure you want to delete " + dataObject.AssetName + "? This action cannot be undone.", "Yes", "No"))
        {
            RemoveFromCollection(dataObject);
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(dataObject));
            EditorUtility.SetDirty(dataCollection);
            AssetDatabase.SaveAssets();
        }
    }

    public void Remove(DataObject dataObject)
    {
        Undo.RegisterCompleteObjectUndo(new UnityEngine.Object[] { dataObject, dataCollection }, "Remove");
        RemoveFromCollection(dataObject);
    }

    private void RemoveFromCollection(DataObject dataObject)
    {
        dataCollection.Remove((TData)dataObject);
        ResetEditorIndices();
    }

    public void MoveUp(DataObject dataObject)
    {
        int oldIndex = Array.IndexOf(dataCollection.Items, dataObject);
        int newIndex = Mathf.Clamp(oldIndex - 1, 0, dataCollection.Items.Length);
        Swap(oldIndex, newIndex);
    }

    private void Swap(int index1, int index2)
    {
        index1 = Mathf.Clamp(index1, 0, dataCollection.Items.Length - 1);
        index2 = Mathf.Clamp(index2, 0, dataCollection.Items.Length - 1);

        TData temp1 = dataCollection.Items[index1];
        TData temp2 = dataCollection.Items[index2];
        dataCollection.Items[index1] = temp2;
        dataCollection.Items[index2] = temp1;

        if (editors.ContainsKey(temp1))
        {
            editors[temp1].Index = index2;
            editors[temp1].ResetIndex();
        }
        if (editors.ContainsKey(temp2))
        {
            editors[temp2].Index = index1;
            editors[temp2].ResetIndex();
        }
        EditorUtility.SetDirty(dataCollection);
    }

    public void MoveDown(DataObject dataObject)
    {
        int oldIndex = Array.IndexOf(dataCollection.Items, dataObject);
        int newIndex = Mathf.Clamp(oldIndex + 1, 0, dataCollection.Items.Length);
        Swap(oldIndex, newIndex);
    }

    public void Move(DataObject dataObject, int index)
    {
        int oldIndex = Array.IndexOf(dataCollection.Items, dataObject);
        Swap(oldIndex, index);
    }

    public int GroupField(int groupIndex)
    {
        if (groupIndex < 0 || groupIndex >= dataCollection.groupNames.Length)
            groupIndex = 0;

        return EditorGUILayout.Popup(groupIndex, dataCollection.groupNames, GUILayout.Width(50));
    }

    protected TData Find(string assetId)
    {
        foreach (var item in dataCollection.Items)
        {
            if (item.AssetID == assetId)
                return item;
        }
        return null;
    }
}