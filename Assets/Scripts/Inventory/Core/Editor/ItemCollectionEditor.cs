﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ItemCollection))]
public class ItemCollectionEditor : DataCollectionEditor<Item, ItemEditor>
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Default Inspector");
        DrawDefaultInspector();
    }
}
