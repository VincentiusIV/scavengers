﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using UnityEngine;

/// <summary>
/// Contains utility functions for finding stuff.
/// </summary>
public static class Utility
{
    public static Type[] FetchTypes<TData>(out string[] typeNames, bool includeSearchType = false)
    {
        IEnumerable<Type> typeEnumerable = Utility.FindDerivedTypes<TData>(includeSearchType);
        List<Type> typeList = new List<Type>();
        List<string> names = new List<string>();
        foreach (var item in typeEnumerable)
        {
            if (item.IsAbstract)
                continue;

            typeList.Add(item);
            names.Add(item.ToString());
        }
        typeNames = names.ToArray();
        return typeList.ToArray();
    }

    public static IEnumerable<Type> FindDerivedTypes<T>(bool includeSearchType = false)
    {
        Type baseType = typeof(T);
        Assembly assembly = baseType.Assembly;
        return assembly.GetTypes().Where(t => ((includeSearchType && t == baseType) || t != baseType) && baseType.IsAssignableFrom(t));
    }

    public static IEnumerable<Type> FindDerivedTypes(Type baseType)
    {
        Assembly assembly = baseType.Assembly;
        return assembly.GetTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t));
    }

}
