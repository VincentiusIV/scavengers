﻿using System;
using System.Collections.Generic;

using UnityEngine;

public class SlotEventArgs : EventArgs
{
    public SlotEventArgs(Slot slot, RuntimeItem previousItem, int previousAmount)
    {
        this.slot = slot;
        this.previousItem = previousItem;
        this.previousAmount = previousAmount;
    }

    public Slot slot { get; }
    public RuntimeItem previousItem { get; }
    public RuntimeItem newItem { get => slot.Contents; }
    public int previousAmount { get; }
    public int newAmount { get => slot.Amount; }
}

public class Slot
{
    public EventHandler<SlotEventArgs> Changed;
    public bool IsEmpty { get { return (Contents == null || Amount == 0); } }
    public RuntimeItem Contents { get; private set; }
    public int Amount { get; private set; }
    public InventoryComponent OwnerInventory;

    public Slot(InventoryComponent ownerInventory)
    {
        OwnerInventory = ownerInventory ?? throw new ArgumentNullException();
    }

    public bool CanPlace(RuntimeItem data, int amount)
    {
        return (CanPlaceData(data) && CanPlaceAmount(amount));
    }

    public virtual bool CanPlaceData(RuntimeItem data)
    {
        return (!Equals(Contents, data));
    }

    public bool CanPlaceAmount(int amount)
    {
        return true;
    }

    public bool SetData(RuntimeItem item, int amount)
    {
        if (!CanPlace(item, amount))
            return false;
        //SlotEventArgs e = new SlotEventArgs(this, Contents, amount);
        SlotEventArgs e = new SlotEventArgs(this, Contents, Amount);
        Contents = item;
        Amount = amount;
        if (Amount <= 0)
            Contents = null;
        InvokeChangedEvent(e);
        //InvokeChangedEvent(e);
        return true;
    }

    private void InvokeChangedEvent(SlotEventArgs e)
    {
        Changed?.Invoke(this, e);
        if (e.previousItem != null && e.newItem == null)
            OnSlotEmptied(e);
        else if (e.previousItem == null && e.newItem != null)
            OnSlotFilled(e);
        else if (e.previousItem != null && e.newItem != null)
        {
            OnSlotEmptied(e);
            OnSlotFilled(e);
        }
    }

    protected virtual void OnSlotEmptied(SlotEventArgs e)
    {

    }

    protected virtual void OnSlotFilled(SlotEventArgs e)
    {

    }

    public void ChangeAmount(int amount = 0)
    {
        int newAmount = this.Amount + amount;
        SetAmount(newAmount);
    }

    public bool SetAmount(int amount)
    {
        if (!CanPlaceAmount(amount))
            return false;
        SlotEventArgs e = new SlotEventArgs(this, Contents, Amount);
        Amount = amount;
        if (Amount <= 0)
            Contents = null;
        InvokeChangedEvent(e);
        return true;
    }

    public static bool SwapContents(Slot slot1, Slot slot2)
    {
        RuntimeItem item1 = slot1.Contents;
        int amount1 = slot1.Amount;
        RuntimeItem item2 = slot2.Contents;
        int amount2 = slot2.Amount;
        if (!slot1.CanSwapWith(slot2))
            return false;
        slot1.SetData(item2, amount2);
        slot2.SetData(item1, amount1);
        return true;
    }

    public virtual bool CanSwapWith(Slot slot)
    {
        bool validSwap = slot.CanPlace(Contents, Amount) && CanPlace(slot.Contents, slot.Amount);
        return validSwap;
    }
}
