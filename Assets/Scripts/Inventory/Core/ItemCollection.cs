﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;


[CreateAssetMenu(fileName = "Item Collection", menuName = "Data/ItemCollection")]
public class ItemCollection : RPGData.DataCollection<Item>
{
    public Item[] items;
    public int maxStackSize = 5;
    public override Item[] Items { get => items; set => items = value; }
}
