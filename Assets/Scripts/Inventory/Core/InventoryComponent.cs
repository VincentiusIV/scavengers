﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

///<summary>
/// Keeps track of collection of items. 
///</summary>
public class InventoryComponent : MonoBehaviour
{
    public event EventHandler<InventoryEventArgs> ItemAdded;
    public event EventHandler<InventoryEventArgs> ItemRemoved;
    public int capacity = 10;
    public int maxStackSize => itemDataCollection.maxStackSize;
    public List<Slot> Slots { get; private set; } = new List<Slot>();
    public ItemCollection itemDataCollection;
    protected PhotonView photonView;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        for (int i = 0; i < capacity; i++)
        {
            Slot newSlot = new Slot(this);
            Slots.Add(newSlot);
        }
        if (itemDataCollection == null)
            itemDataCollection = Resources.Load<ItemCollection>("Item Collection");
        itemDataCollection.Init();
    }

    private void Start()
    {
        Actor actor = GetComponent<Actor>();
        if (actor != null && photonView.IsMine)
        {
            actor.OnKilled.AddListener(OnKilled);
        }
    }

    private void OnKilled()
    {
        foreach (var slot in Slots)
        {
            DropSlotContents(slot);
        }
    }

    public bool AddItemByID(string id, int amount = 1, int preferredSlotId = -1)
    {
        if (id == null)
            return false;
        photonView.RPC("AddItemByIDRpc", RpcTarget.All, id, amount, preferredSlotId);
        return true;
    }

    [PunRPC]
    private void AddItemByIDRpc(string id, int amount, int preferredSlotId)
    {
        if (id == null || itemDataCollection == null)
        {
            Debug.LogError("Null id or collection!");
            return;
        }
        Item item = (itemDataCollection.Find(id));
        if (item == null) Debug.LogErrorFormat("Unable to find asset with id: {0}", id);
        AddItem(item, amount, preferredSlotId);
    }

    private bool AddItem(Item itemToAdd, int amount, int preferredSlotId)
    {
        Debug.Assert(itemToAdd != null);
        if (itemToAdd == null || amount <= 0)
            return false;
        RuntimeItem runtimeItem = new RuntimeItem(itemToAdd);
        runtimeItem.State = ItemState.NEW;        
        return AddItem(runtimeItem, amount, preferredSlotId);
    }

    private bool AddItem(RuntimeItem runtimeItem, int amount, int preferredSlotId)
    {
        Debug.Assert(runtimeItem != null);
        if (runtimeItem == null || amount <= 0)
            return false;
        Item itemToAdd = runtimeItem.Item;
        Slot slot = null; 
        if(!TryStackingItem(amount, itemToAdd, ref slot))
        {
            for (int i = 0; i < amount; i++)
            {
                slot = GetEmptySlot(preferredSlotId);
                if (slot == null)
                    return false;
                if(itemToAdd.stackable)
                {
                    InsertIntoSlot(runtimeItem, amount, slot);
                    break;
                }
                else
                {
                    InsertIntoSlot(runtimeItem, 1, slot);
                }
            }
        }

        InventoryEventArgs e = new InventoryEventArgs(null, LogEventType.GAINED, runtimeItem, slot);
        GameLog.DispatchEvent(this, ItemAdded, e);
        return true;
    }

    private bool TryStackingItem(int amount, Item itemToAdd, ref Slot slot)
    {
        if (ContainsItem(itemToAdd.AssetID) && itemToAdd.Stackable)
        {
            slot = FindFirstSlotWithItem(itemToAdd.AssetID, true);
            if (slot == null)
                return false;
            slot.ChangeAmount(amount);
            return true;
        }
        return false;
    }

    private void InsertIntoSlot(RuntimeItem runtimeItem, int amount, Slot slot)
    {
        if (amount <= maxStackSize)
        {
            slot.SetData(runtimeItem, amount);
        }
        else
        {
            slot.SetData(runtimeItem, maxStackSize);
            AddItem(runtimeItem.Item, amount - maxStackSize, -1);
        }
    }

    public bool DropSlotContents(Slot slot)
    {
        if (slot.IsEmpty)
            return false;
        Item item = slot.Contents.Item;
        int amount = slot.Amount;
        GameObject pickup;
        if(item.pickupObject != null)
            pickup = PhotonNetwork.Instantiate(item.pickupObject.name, transform.position + new Vector3(0, 1.5f, 0) + transform.forward, transform.rotation);
        else
            pickup = PhotonNetwork.Instantiate(ScavGameManager.Instance.gameSettings.defaultItemPickup.name, transform.position + new Vector3(0, 1.5f, 0) + transform.forward, transform.rotation);
        pickup.GetComponent<Rigidbody>().AddForce(transform.forward * 10f);
        pickup.GetComponent<PickupReaction>().SetData(item.AssetID, amount);
        return RemoveItemByID(item.AssetID, amount, SlotIdx(slot));
    }

    public bool RemoveSpecificItem(RuntimeItem runtimeItem)
    {
        List<Slot> allStacks = GetAllSlotsOfItem(runtimeItem.Item.AssetID);
        bool result = false;
        foreach (var slot in allStacks)
        {
            if(slot.Contents == runtimeItem)
            {
                slot.ChangeAmount(-1);
                InventoryEventArgs e = new InventoryEventArgs(null, LogEventType.LOST, runtimeItem, slot);
                GameLog.DispatchEvent(this, ItemRemoved, e);
                result = true;
            }
        }

        return result;
    }

    public bool RemoveItemByID(string itemId, int amount = 1, int prefferedSlotId = -1)
    {
        if (itemId == null)
            return false;
        photonView.RPC("RemoveItemRpc", RpcTarget.All, itemId, amount, prefferedSlotId);
        return true;
    }

    [PunRPC]
    private void RemoveItemRpc(string itemId, int amount, int prefferedSlotId)
    {
        if (GetTotalAmountOfItem(itemId) < amount)
            return;
        if (prefferedSlotId >= 0 && prefferedSlotId < Slots.Count)
            amount = RemoveContents(amount, Slots[prefferedSlotId]);
        List<Slot> allSlotsOfItem = GetAllSlotsOfItem(itemId);
        foreach (var slot in allSlotsOfItem)
        {
            if (amount == 0) break;
            amount = RemoveContents(amount, slot);
        }
        return;
    }

    private int RemoveContents(int amount,  Slot slot)
    {
        int amountToRemove = Mathf.Min(slot.Amount, amount);
        InventoryEventArgs e = new InventoryEventArgs(null, LogEventType.LOST, slot.Contents, slot);
        GameLog.DispatchEvent(this, ItemRemoved, e);
        slot.ChangeAmount(-amountToRemove);
        amount -= amountToRemove;
        return amount;
    }

    public bool MoveItemFromTo(Slot stack1, Slot stack2)
    {
        if(stack1.OwnerInventory == stack2.OwnerInventory)
            return Slot.SwapContents(stack1, stack2);
        else
        {
            string itemId1 = stack1.Contents?.Item.AssetID;
            int itemAmount1 = stack1.Amount;
            int slotId1 = stack1.OwnerInventory.SlotIdx(stack1);
            string itemId2 = stack2.Contents?.Item.AssetID;
            int itemAmount2 = stack2.Amount;
            int slotId2 = stack2.OwnerInventory.SlotIdx(stack2);
            stack1.OwnerInventory.RemoveItemByID(itemId1, itemAmount1);
            stack1.OwnerInventory.AddItemByID(itemId2, itemAmount2, slotId1);
            stack2.OwnerInventory.RemoveItemByID(itemId2, itemAmount2);
            stack2.OwnerInventory.AddItemByID(itemId1, itemAmount1, slotId2);
            return true;
        }
    }

    private Slot GetEmptySlot(int prefferedSlotId = -1)
    {
        if (prefferedSlotId >= 0 && prefferedSlotId < Slots.Count && Slots[prefferedSlotId].IsEmpty)
            return Slots[prefferedSlotId];
        return Slots.FirstOrDefault(s => s.IsEmpty);
    }

    public Slot FindFirstSlotWithItem(string itemId, bool ignoreFull = false)
    {
        foreach (var slot in Slots)
        {
            if (slot.IsEmpty)
                continue;
            if(slot.Contents.Item.AssetID == itemId)
            { 
                if(ignoreFull && slot.Amount >= maxStackSize)
                    continue;                
                return (slot);
            }
        }
        return (null);
    }

    public int GetTotalAmountOfItem(string itemID)
    {
        int amount = 0;
        foreach (var slot in Slots)
        {
            if (!slot.IsEmpty && slot.Contents.Item.AssetID == itemID)
                amount += slot.Amount;
        }
        return amount;
    }

    public List<Slot> GetAllSlotsOfItem(string itemID)
    {
        List<Slot> allStacks = new List<Slot>();
        Item item = itemDataCollection.Find(itemID);
        Debug.Assert(item != null, "Item of ID " + itemID + " does not exist");
        foreach (var slot in Slots)
            if (!slot.IsEmpty && slot.Contents.Item.AssetID == itemID)
                allStacks.Add(slot);
        return (allStacks);
    }

    public Slot FindFirstWeapon()
    {
        foreach (var slot in Slots)
            if (!slot.IsEmpty &&slot.Contents.Item.itemType == ItemType.WEAPON)
                return slot;
        return null;
    }

    public List<Slot> FindAllSlotsWithItemType(ItemType itemType)
    {
        List<Slot> slots = new List<Slot>();
        foreach (var slot in Slots)
            if (!slot.IsEmpty && slot.Contents.Item.Type == itemType)
                slots.Add(slot);
        return slots;
    }

    public bool IsFull()
    {
        bool result = true;
        foreach (var slot in Slots)
            result &= !slot.IsEmpty;
        return result;
    }

    public bool CanAddItem(Item item)
    {
        bool result = false;
        foreach (var slot in Slots)
        {
            result |= slot.IsEmpty || (!slot.IsEmpty && slot.Contents.Item == item && slot.Amount < maxStackSize);
            if (result) break;
        }
        return result;
    }

    public bool ContainsItem(string itemId)
    {
        bool contains = false;
        foreach (var slot in Slots)
            if (!slot.IsEmpty)
                contains |= slot.Contents.Item.AssetID == itemId;
        return contains;
    }
    
    public void Clear()
    {
        Slots.ForEach(s => s.SetData(null, 0));
    }

    public void SetEnabled(bool enabled)
    {

    }

    private int SlotIdx(Slot slot)
    {
        for (int i = 0; i < Slots.Count; i++)
        {
            if (Slots[i] == slot)
                return i;
        }
        return -1;
    }
}
