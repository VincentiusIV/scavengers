﻿using RPGData;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public enum ItemType
{
    DEFAULT = 0,
    WEAPON = 17,
    CONSUMABLE = 20
}

public class Item : DataObject
{
    public string Name { get => itemName; set => itemName = value; }
    public string Description { get => description; set => description = value; }
    public virtual ItemType Type { get => itemType; set => itemType = value; }
    public float Weight { get => weight; set => weight = value; }
    public bool Stackable { get => stackable; set => stackable = value; }
    public string IconSlug { get => iconSlug; set { iconSlug = value; } }
    public string WorldObjectSlug { get => worldObjectSlug; set { worldObjectSlug = value; } }
    public Sprite Icon { get => icon; set => icon = value; }
    public GameObject Equipable { get => equipable; set => equipable = value; }
    public GameObject PickupObject { get => pickupObject; }
    public bool IsEquipment { get => (int)itemType > 10 && (int)itemType < 20; }

    public string itemName;
    public string description = "Item description...";
    public ItemType itemType;
    public float weight;
    public bool stackable;
    public Sprite icon;
    [UnityEngine.Serialization.FormerlySerializedAs("worldObject")] public GameObject equipable;
    public GameObject pickupObject;
    [HideInInspector] private string iconSlug;
    [HideInInspector] private string worldObjectSlug;

    public override string AssetName { get => Name; set { Name = value; UpdateID(); } }
}

