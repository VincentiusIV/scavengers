﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
public class LootItem
{
    public Item item;
    [Range(0f, 100f)]
    public float dropChance = 100f;
    public int minAmount = 1;
    public int maxAmount = 1;

    public int dropAmount { get; set; }

    public LootItem()
    {
        dropChance = 0f;
        minAmount = maxAmount = 0;
    }

    public LootItem(Item item, float dropChance, int minAmount, int maxAmount)
    {
        this.item = item;
        this.dropChance = dropChance;
        this.minAmount = minAmount;
        this.maxAmount = maxAmount;
    }

    public void Determine()
    {
        bool willDrop = (dropChance >= Random.Range(0f, 100f));
        dropAmount = willDrop ? Random.Range(minAmount, maxAmount + 1) : 0;
    }
}
