﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class InventoryEventArgs : LogEvent
{
    public RuntimeItem RuntimeItem { get; }
    public Slot Slot { get; }

    public InventoryEventArgs(Actor Actor, LogEventType EventType, RuntimeItem RuntimeItem, Slot Slot) : base(Actor, EventType)
    {
        Debug.Assert(RuntimeItem != null);
        
        this.RuntimeItem = RuntimeItem;
        this.Slot = Slot;
    }

    public override string Print()
    {
        return string.Format("{0} {1} {2}", SourceActor?.photonView.Owner.NickName, EventType.ToString(), RuntimeItem.Item.Name);
    }
}
