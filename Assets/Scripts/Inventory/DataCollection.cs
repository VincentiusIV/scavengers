﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RPGData
{
    public abstract class DataCollection<TData> : DataObject, IDataCollection
        where TData : DataObject
    {
        public string resourcePath;
        public abstract TData[] Items { get; set; }
        DataObject[] IDataCollection.Items => Items;
        public string[] groupNames = new string[0];
        private Dictionary<string, TData> sortedCollection;

        public override void Init()
        {
            base.Init();
            sortedCollection = new Dictionary<string, TData>();
            if (Items == null)
                Items = new TData[0];
            foreach (var item in Items)
            {
                if (!sortedCollection.ContainsKey(item.AssetID))
                    sortedCollection.Add(item.AssetID, item);
            }
        }

        public TData Find(string assetID)
        {
            if(assetID == null)
            {
                Debug.LogError("Cannot find asset with null asset id");
                return null;
            }
            if (sortedCollection == null || sortedCollection.Count == 0)
                Init();
            if (sortedCollection.ContainsKey(assetID))
                return sortedCollection[assetID];
            return null;
        }

        public string CreateUniquePath(TData asset)
        {
            return string.Format("Assets/Resources/{0}/{1}.asset", resourcePath, asset.AssetID);
        }

#if UNITY_EDITOR
        public virtual void Add(TData target)
        {
            List<TData> temp = new List<TData>();
            temp.AddRange(Items);
            temp.Add(target);
            Items = temp.ToArray();
            EditorUtility.SetDirty(this);
        }

        public virtual void Remove(TData target)
        {
            List<TData> temp = new List<TData>();
            temp.AddRange(Items);
            if(!temp.Contains(target))
                return;
            temp.Remove(target);
            Items = temp.ToArray();
            EditorUtility.SetDirty(this);
        }

        public void RemoveAndDelete(TData target)
        {
            Remove(target);
            string targetPath = AssetDatabase.GetAssetPath(target);
            AssetDatabase.DeleteAsset(targetPath);
            AssetDatabase.SaveAssets();
        }

        public TData Create()
        {
            return Create(typeof(TData));
        }

        public TData Create(Type type)
        {
            TData asset;
            asset = (TData)CreateInstance(type);
            asset.Init();
            string path = CreateUniquePath(asset);
            if (AssetDatabase.LoadAssetAtPath<TData>(path) == null)
            {
                AssetDatabase.CreateAsset(asset, path);
                Add(asset);
                EditorUtility.SetDirty(this);
                EditorUtility.SetDirty(asset);
                AssetDatabase.SaveAssets();
            }
            else
            {
                Debug.LogErrorFormat("Failed to create {0} since an asset exists at path {1}", type?.ToString(), path);
            }
            return asset;
        }
#endif
    }
}

