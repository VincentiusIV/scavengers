﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

[System.Serializable]
public class TravelStage
{
    public string text = "New Message";
    public AudioClip voiceClip;
    public float waitAfterClip = 1;
}

[RequireComponent(typeof(PhotonView))]
public class Shipwreck : MonoBehaviour
{
    public UnityEvent OnEnter;
    public UnityEvent OnExit;
    public GameObject root;
    public Transform shipJoint;
    
    public TravelStage[] travelStagesOnEnter, travelStagesOnExit;

    private PhotonView photonView;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    public IEnumerator Enter()
    {
        root.SetActive(true);
        OnEnter.Invoke();
        yield return HandleTravelStages(travelStagesOnEnter);
    }

    private IEnumerator HandleTravelStages(TravelStage[] travelStages)
    {
        foreach (var stage in travelStages)
        {
            yield return SpaceshipManager.Instance.PlayShipAIVoice(stage.text, stage.voiceClip);
            yield return new WaitForSeconds(stage.waitAfterClip);
        }
    }

    public IEnumerator Exit()
    {
        root.SetActive(false);
        OnExit.Invoke();
        yield return HandleTravelStages(travelStagesOnExit);
    }
}
