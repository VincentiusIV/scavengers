﻿using System;
using System.Collections.Generic;

using UnityEngine;

public enum LogEventType
{
    UNDEFINED = 0,
    ATTACKED = 1,
    KILLED = 2,
    RESPAWNED = 3,
    DEFEATED = 4,
    GAINED = 10,
    LOST = 11,
    EQUIPPED = 20,
    UNEQUIPPED = 21,
    ZONE_ENTER = 60,
    ZONE_EXIT = 61,
}

public class LogEvent : EventArgs
{
    public Actor SourceActor { get; }
    public LogEventType EventType { get; }

    public LogEvent(Actor SourceActor, LogEventType EventType)
    {
        this.SourceActor = SourceActor;
        this.EventType = EventType;
    }

    public virtual string Print()
    {
        return ToString();
    }
}


public class GameLogEventArgs : EventArgs
{
    public GameLogEventArgs(LogEvent logEvent)
    {
        LogEvent = logEvent;
    }

    public LogEvent LogEvent { get; }

}

/// <summary>
/// Provides a centralized way to dispatch and track any type of event within the RPG game.
/// </summary>
public class GameLog : MonoBehaviour
{
    public static EventHandler<GameLogEventArgs> OnLogAdded;   
    private static GameLog Instance { get; set; }
    private List<LogEvent> log = new List<LogEvent>();
    public static bool logToConsole = true;
    public static bool friendlyKilling { get; private set; } = false;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
        }
    }

    public static void Add(LogEvent e)
    {
        if (Instance != null)
            Instance.AddLog(e);
    }

    public void AddLog(LogEvent e)
    {
        if (Instance == null || !enabled)
            return;

        log.Add(e);

        GameLogEventArgs glEventArgs = new GameLogEventArgs(e);
        try
        {
            if (OnLogAdded != null)
                OnLogAdded.Invoke(Instance, glEventArgs);
        }
        catch(Exception exception)
        {
            Debug.LogError(exception);
        }

        if(logToConsole && e != null && e.EventType != LogEventType.ZONE_ENTER && e.EventType != LogEventType.ZONE_EXIT)
            Debug.Log(string.Format("{0} at {1}: {2}", e.GetType().ToString(), DateTime.Now.ToShortTimeString(), e.Print()));
    }

    public static void SetEnabled(bool enabled)
    {
        Instance.enabled = enabled;
    }

    public static void LogToConsole()
    {
        logToConsole = true;
    }

    public static void HideFromConsole()
    {
        logToConsole = false;
    }

    public static void EnableFriendlyKilling()
    {
        friendlyKilling = true;
    }

    public static void DisableFriendlyKilling()
    {
        friendlyKilling = false;
    }

    public static void DispatchEvent<T>(object source, EventHandler<T> handler, T eventArgs)
        where T : LogEvent
    {
        if (handler != null)
        {
            handler.Invoke(source, eventArgs);
        }

        Add(eventArgs);
    }

    private void OnDestroy()
    {
        Instance = null;
        
    }
}
