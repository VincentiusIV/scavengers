﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;
using System;

public class SpaceshipManager : MonoBehaviour
{
    public static UnityEvent<string> OnAISpeak;
    public static SpaceshipManager Instance;
    public ShipwreckManager shipwreckManager;
    public GameSettings gameSettings;
    public TakeItemReaction tanker;
    public Door airlock;
    public Transform root;
    public AudioSource shipAIVoiceSource;
    private List<Scavengers> scavsInSpaceship = new List<Scavengers>();

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
        OnAISpeak = new UnityEvent<string>();
    }

    public void RestartGame()
    {
        shipwreckManager.GoToFirstShipwreck();
    }

    public void LeaveShipwreck(bool debug = false)
    {
        if (tanker.taken == 0 && !debug)
        {
            InfoTextDisplay.Display("Insufficient fuel");
            return;
        }
        
        if(!AreAllPlayersInShip())
        {
            Debug.Log("[Spaceship]: Not all players are in ship and thus we cannot leave...");
            InfoTextDisplay.Display("Not all players are on board");
            return;
        }
        
        airlock.Close();
        shipwreckManager.GoToNextShipwreck();
    }

    private bool AreAllPlayersInShip()
    {
        foreach (var scav in Scavengers.scavengers)
            if(scav.IsAlive && !scavsInSpaceship.Contains(scav))
                return false;
        return true;
    }

    private void OnTriggerEnter(Collider other)
    {
        Scavengers scav = other.GetComponentInParent<Scavengers>();
        if(scav != null)
        {
            scav.flashlight.Reload();
            if(!scavsInSpaceship.Contains(scav))
                scavsInSpaceship.Add(scav);
        }
    }

    public void TeleportShip(Transform shipJoint)
    {
        Dictionary<Scavengers, Vector3> jointRelativePositions = new Dictionary<Scavengers, Vector3>();
        foreach (var scav in Scavengers.scavengers)
        {
            Vector3 pos = root.InverseTransformPoint(scav.transform.position);
            jointRelativePositions.Add(scav, pos);
        }

        root.position = shipJoint.position;
        root.rotation = shipJoint.rotation;

        foreach (var scav in Scavengers.scavengers)
        {
            if(scav.photonView.IsMine)
            {
                Vector3 pos = root.TransformPoint(jointRelativePositions[scav]);
                scav.transform.position = pos;
            }

            if(!scav.IsAlive)
            {
                scav.Respawn();
                int spawnIndex = Scavengers.scavengers.IndexOf(scav);
                scav.transform.position = ScavGameManager.GetSpawnPosition(spawnIndex);
            }
        }
    }

    public IEnumerator PlayShipAIVoice(string text, AudioClip clip)
    {
        if(text != null)
            OnAISpeak.Invoke(text);

        if(clip != null)
        {
            shipAIVoiceSource.clip = clip;
            shipAIVoiceSource.Play();
            yield return new WaitUntil(() => shipAIVoiceSource.isPlaying);
            yield return new WaitUntil(() => !shipAIVoiceSource.isPlaying);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Scavengers scav = other.GetComponentInParent<Scavengers>();
        if (scav != null && scavsInSpaceship.Contains(scav))
            scavsInSpaceship.Remove(scav);
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            OnAISpeak.RemoveAllListeners();
            Instance = null;
        }
    }

    public void Lock()
    {
        airlock.Close();
        airlock.Lock();
    }

    public void Unlock()
    {
        airlock.Unlock();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Home))
        {
            LeaveShipwreck(true);
        }
    }
#endif
}
