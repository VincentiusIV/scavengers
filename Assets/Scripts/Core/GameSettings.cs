﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Game Settings")]
public class GameSettings : ScriptableObject
{
    public string[] sceneNames = new string[1] { "CombinedScene" };
    public ItemCollection itemCollection;
    public GameObject defaultItemPickup;
    public GameObject[] prefabs;

    private int sceneIdx = 0;
    public string CurrentScene { get { return sceneNames[sceneIdx]; }  }
    public void NextScene() { ++sceneIdx; sceneIdx = sceneIdx % sceneNames.Length; }
    public void ResetScenes() { sceneIdx = 0; }
}

