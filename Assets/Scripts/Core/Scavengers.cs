﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public enum ScavMode
{
    PLAYER,
    OTHER
}

public class Scavengers : Actor
{
    public static List<Scavengers> scavengers = new List<Scavengers>();
    public Camera playerCamera;
    public Transform hipsTransform;
    public Flashlight flashlight;
    public float cameraPosLerp = 10f;
    public Vector3 standCameraOffset = new Vector3(0, -.12f, .12f);
    public Vector3 crouchCameraOffset = new Vector3(0.15f, 0.9f, 0.24f);
    public Vector3 deathCameraRotationAdjust = new Vector3(-90f, 90f, 180f);
    private Vector3 cameraOffset;
    private InteractionComponent interaction;
    private EquipmentComponent equipment;
    private Vector2 rotation;
    private Transform cameraFollowTf;
    private Vector3 lastCameraPos;
    public float minXRotation = -60f, maxXRotation = 45f;
    private float cameraHeight;

    private void Start()
    {
        scavengers.Add(this);
        interaction = GetComponent<InteractionComponent>();
        equipment = GetComponent<EquipmentComponent>();

        if (photonView.IsMine)
            playerCamera.GetComponent<SpectateCamera>().Enable();
        else
            playerCamera.GetComponent<SpectateCamera>().Disable();

        cameraFollowTf = playerCamera.transform.parent;
        playerCamera.transform.SetParent(null);
        flashlight.Setup(null, this);

        if (photonView.IsMine)
        {
            UIManager.Instance.Observe(this);
        }
    }

    private void Update()
    {
        if (photonView.IsMine && CanControl && IsAlive)
        {
            Vector3 force = (Vector3.forward) * Input.GetAxisRaw("Vertical") + (Vector3.right * Input.GetAxisRaw("Horizontal"));
            force = Vector3.ProjectOnPlane(force, Vector3.up);
            force.Normalize();
            force *= move.moveSpeed * Time.deltaTime * 1000.0f;
            move.Move(force);

            float yRotation = rotation.y + Input.GetAxisRaw("Mouse X") * Time.deltaTime * 100.0f * move.rotateSpeed;
            rotation.y = Mathf.Lerp(rotation.y, yRotation, cameraPosLerp);

            float xRotation = rotation.x - Input.GetAxisRaw("Mouse Y") * Time.deltaTime * 100.0f * move.rotateSpeed;
            rotation.x = Mathf.Lerp(rotation.x, xRotation, cameraPosLerp);
            rotation.x = Mathf.Clamp(rotation.x, minXRotation, maxXRotation);

            cameraHeight = Mathf.Lerp(cameraHeight, cameraFollowTf.transform.position.y, cameraPosLerp * Time.deltaTime);
            cameraOffset = Vector3.Lerp(cameraOffset, move.IsCrouching ? crouchCameraOffset : standCameraOffset, cameraPosLerp * Time.deltaTime);

            photonView.RPC("ApplyMovementInput", RpcTarget.Others, rotation, cameraOffset, cameraHeight);

            Vector3 lookDir = playerCamera.transform.forward.normalized;
            lookDir.y = 0f;
            Quaternion newRotation = Quaternion.LookRotation(lookDir);
            move.Rigidbody.MoveRotation(newRotation);
            transform.rotation = newRotation;

            if (Input.GetKeyDown(KeyCode.T) || flashlight.ShouldDeactivate())
                photonView.RPC("SwitchFlashlightRpc", RpcTarget.All);
            if (Input.GetKeyDown(KeyCode.C))
                photonView.RPC("SwitchCrouchRpc", RpcTarget.All);
            if (Input.mouseScrollDelta.y > 0)
                photonView.RPC("NextHotbarRpc", RpcTarget.All);
            if (Input.mouseScrollDelta.y < 0)
                photonView.RPC("PreviousHotbarRpc", RpcTarget.All);
        }

        UpdatePlayerCamera();

        if (photonView.IsMine)
        {
            if (IsAlive)
            {
                if (Input.GetKeyDown(KeyCode.E) || (interaction.state == InteractionState.ACTIVE && Input.GetKeyDown(KeyCode.Escape)))
                    interaction.SwitchInteractWithNearest();
                if (Input.GetKeyDown(KeyCode.I))
                {
                    interaction.Break();
                    UIManager.Instance.SwitchScreen(typeof(ItemExchangeUI));
                }
                if (UIManager.Instance.IsScreenVisible(typeof(ItemExchangeUI)) && Input.GetKeyDown(KeyCode.Escape))
                    UIManager.HideScreen(typeof(ItemExchangeUI));
            }

            if (Input.GetKeyDown(KeyCode.F5))
                Hit();
            if (Input.GetKeyDown(KeyCode.F6))
                Respawn();

            Cursor.visible = !CanControl;
            Cursor.lockState = CanControl ? CursorLockMode.Locked : CursorLockMode.None;
        }
    }

    private void UpdatePlayerCamera()
    {
        if (IsAlive)
        {
            playerCamera.transform.rotation = Quaternion.Euler(new Vector3(rotation.x, rotation.y, 0f));
            playerCamera.transform.position = new Vector3(move.Rigidbody.position.x, cameraHeight, move.Rigidbody.position.z);
            playerCamera.transform.position = playerCamera.transform.TransformPoint(cameraOffset);

        }
        else
        {
            playerCamera.transform.rotation = cameraFollowTf.rotation * Quaternion.Euler(deathCameraRotationAdjust);
            playerCamera.transform.position = cameraFollowTf.position;
            cameraOffset = Vector3.Lerp(cameraOffset, move.IsCrouching ? crouchCameraOffset : standCameraOffset, cameraPosLerp * Time.deltaTime);
            playerCamera.transform.position = playerCamera.transform.TransformPoint(cameraOffset);
        }
    }

    [PunRPC]
    private void ApplyMovementInput(Vector2 rotation, Vector3 cameraOffset, float cameraHeight)
    {
        this.rotation = rotation;
        this.cameraOffset = cameraOffset;
        this.cameraHeight = cameraHeight;
    }

    [PunRPC]
    private void SwitchFlashlightRpc()
    {
        flashlight.Switch();
    }

    [PunRPC]
    private void SwitchCrouchRpc()
    {
        move.SwitchCrouch();
    }

    [PunRPC]
    private void NextHotbarRpc()
    {
        equipment.Hotbar.Next();
    }

    [PunRPC]
    private void PreviousHotbarRpc()
    {
        equipment.Hotbar.Previous();
    }

    private void LateUpdate()
    {
        if (!photonView.IsMine)
        {
            Vector3 euler = hipsTransform.transform.localEulerAngles;
            float norm = Mathf.InverseLerp(minXRotation, maxXRotation, rotation.x);
            euler.z = Mathf.Lerp(minXRotation, maxXRotation, 1f - norm);
            hipsTransform.transform.localRotation = Quaternion.Euler(euler);
        }
    }

    private void OnDestroy()
    {
        scavengers.Remove(this);
        if(playerCamera != null)
            Destroy(playerCamera.gameObject);
    }
}
