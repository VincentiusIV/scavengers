﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class TriggerEventArgs : EventArgs
{
    public Collider Collider { get; set; }
}

public class TriggerEventListener : MonoBehaviour
{
    public EventHandler<TriggerEventArgs> ProximityEnter;
    public EventHandler<TriggerEventArgs> ProximityExit;
    public List<Collider> withinTrigger;
    public float searchInterval = 0.5f;
    public float radius = 3.24f;
    public int bufferSize = 10;
    public bool ignoreTriggers = true, ignoreSelf = true, useTrigger = true;
    public LayerMask searchMask;
    private Collider[] colliderBuffer;
    private HashSet<Collider> insideBuffer = new HashSet<Collider>();
    private IEnumerator searchRoutine;

    protected virtual void Awake()
    {
        withinTrigger = new List<Collider>();
        colliderBuffer = new Collider[bufferSize];
    }

    private void OnEnable()
    {
        if(!useTrigger)
            StartSearchRoutine();
    }

    private void StartSearchRoutine()
    {
        if (searchRoutine != null)
            StopCoroutine(searchRoutine);
        searchRoutine = SearchRoutine();
        StartCoroutine(searchRoutine);
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < withinTrigger.Count; i++)
        {
            if(withinTrigger[i] == null)
            {
                withinTrigger.RemoveAt(i--);
            }
        }
    }

    private IEnumerator SearchRoutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(searchInterval);
            yield return new WaitForFixedUpdate();
            Physics.OverlapSphereNonAlloc(transform.position, radius, colliderBuffer, searchMask);
            insideBuffer.Clear();
            
            foreach (var collider in colliderBuffer)
            {
                if (collider == null || ignoreSelf && collider.gameObject == gameObject || ignoreTriggers && collider.isTrigger)
                    continue;

                insideBuffer.Add(collider);
                if(!withinTrigger.Contains(collider))
                {
                    withinTrigger.Add(collider);
                    Dispatch(ProximityEnter, collider);
                }
            }

            for (int i = 0; i < withinTrigger.Count; i++)
            {
                Collider current = withinTrigger[i];
                if(!insideBuffer.Contains(current))
                {
                    withinTrigger.Remove(current);
                    Dispatch(ProximityExit, current);
                    --i;
                }
            }
        }
    }

    private void Dispatch(EventHandler<TriggerEventArgs> handler, Collider collider)
    {
        if(handler != null)
        {
            TriggerEventArgs e = new TriggerEventArgs();
            e.Collider = collider;
            handler(this, e);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!withinTrigger.Contains(other))
            withinTrigger.Add(other);
        Dispatch(ProximityEnter, other);
    }

    private void OnTriggerExit(Collider other)
    {
        if(withinTrigger.Contains(other))
            withinTrigger.Remove(other);
        Dispatch(ProximityExit, other);
    }

    private void OnDrawGizmosSelected()
    {
        if (useTrigger)
            return;
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
