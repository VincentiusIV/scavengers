﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[System.Serializable]
public class ObjectGroup
{
    public List<PhotonView> objects;
    public int minAlive = 1;
}

public class RandomAlive : MonoBehaviour
{
    public ObjectGroup[] objectGroups;

    private void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
            return;
        foreach (var objectGroup in objectGroups)
        {
            int amountToKill = objectGroup.objects.Count - objectGroup.minAlive;
            for (int i = 0; i < amountToKill; i++)
            {
                int randIdx = Random.Range(0, objectGroup.objects.Count);
                PhotonNetwork.Destroy(objectGroup.objects[randIdx]);
                objectGroup.objects.RemoveAt(randIdx);
            }
        }
    }
}
