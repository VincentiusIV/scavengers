﻿using System;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(InventoryComponent))]
public class ContainerReaction : Reaction
{
    public LootItem[] contents;
    private InventoryComponent inventory;

    protected override void Awake()
    {
        base.Awake();
        inventory = GetComponent<InventoryComponent>();
        ScavGameManager.OnMasterGameStart += OnMasterGameStart;
    }

    private void OnMasterGameStart(object sender, EventArgs e)
    {
        for (int i = 0; i < contents.Length; i++)
        {
            if (contents[i] == null)
                continue;
            contents[i].Determine();
            inventory.AddItemByID(contents[i].item.AssetID, contents[i].dropAmount);
        }
    }

    protected override void OnInteract(Actor actor)
    {
        base.OnInteract(actor);

        UIManager.ShowScreen(typeof(ItemExchangeUI));
        ItemExchangeUI.ShowOther(inventory);
    }

    protected override void OnDisconnect(Actor actor)
    {
        base.OnDisconnect(actor);

        UIManager.HideScreen(typeof(ItemExchangeUI));
    }
}
