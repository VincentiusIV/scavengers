﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public abstract class Reaction : MonoBehaviour
{
    protected Interactable interactable;
    protected PhotonView photonView;

    protected virtual void Awake()
    {
        photonView = GetComponent<PhotonView>();
        interactable = GetComponent<Interactable>();
        interactable.OnSelectEvent.AddListener(OnSelect);
        interactable.OnDeselectEvent.AddListener(OnDeselect);
        interactable.OnInteractEvent.AddListener(OnInteract);
        interactable.OnDisconnectEvent.AddListener(OnDisconnect);
    }

    public virtual bool CanInteract(Actor actor, out string infoMsg)
    {
        infoMsg = "";
        return true;
    }

    protected virtual void OnDisconnect(Actor actor)
    {
    }

    protected virtual void OnInteract(Actor actor)
    {
    }

    protected virtual void OnDeselect(Actor actor)
    {
    }

    protected virtual void OnSelect(Actor actor)
    {
    }
}
