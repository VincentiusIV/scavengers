﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItemReaction : Reaction
{
    public Item item;
    public int amount;

    protected override void OnInteract(Actor actor)
    {
        base.OnInteract(actor);

        InventoryComponent inventory = actor.GetComponent<InventoryComponent>();
        inventory.AddItemByID(item.AssetID, amount);
    }
}
