﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionState
{
    ACTIVE, INACTIVE, UNAVAILABLE
}

public class InteractionComponent : MonoBehaviour
{
    public EventHandler Activated;
    public EventHandler Deactivated;
    public InteractionState state { get; private set; }
    private List<Interactable> nearbyInteractables;
    public Interactable Nearest { get; private set; }
    public Interactable Current { get; private set; }
    private Actor owner;
    public bool SwitchOutOfZoneInteractables = true;
    public TriggerEventListener triggerEventListener;

    private void Awake()
    {
        if (!triggerEventListener)
        {
            Debug.LogError("Cannot initialize interaction component without trigger event listener!", gameObject);
            enabled = false;
            return;
        }

        owner = GetComponent<Actor>();
        state = InteractionState.UNAVAILABLE;
        nearbyInteractables = new List<Interactable>();
        triggerEventListener.ProximityEnter += OnTriggerEnterEvent;
        triggerEventListener.ProximityExit += OnTriggerExitEvent;
    }

    private void OnTriggerEnterEvent(object sender, TriggerEventArgs e)
    {
        Interactable interactable = e.Collider.GetComponentInParent<Interactable>();
        if (interactable != null && !nearbyInteractables.Contains(interactable))
        {
            nearbyInteractables.Add(interactable);
        }
    }

    private void OnTriggerExitEvent(object sender, TriggerEventArgs e)
    {
        Interactable interactable = e.Collider.GetComponentInParent<Interactable>();
        if (interactable != null && nearbyInteractables.Contains(interactable))
        {
            nearbyInteractables.Remove(interactable);

            if (interactable == Current && SwitchOutOfZoneInteractables)
                Break();
        }
    }

    public bool SwitchInteractWithNearest()
    {
        return SwitchInteractWith(Nearest);
    }

    public bool SwitchInteractWith(Interactable interactable)
    {
        switch (state)
        {
            case InteractionState.ACTIVE:
                Break();
                return true;
            case InteractionState.INACTIVE:
                InteractWith(interactable);
                return true;
            case InteractionState.UNAVAILABLE:
            default:
                return false;
        }
    }

    public void Break()
    {
        if (state != InteractionState.ACTIVE)
            return;
        state = InteractionState.INACTIVE;
        if (Current == null)
            return;
        Current.Disconnect();
    }

    private void InteractWith(Interactable interactable)
    {
        if (interactable != null)
        {
            Current = interactable;
            Current.OnDisconnectEvent.AddListener(OnDisconnected);
            string infoTxt = "";
            if (Current.Interact(owner, out infoTxt))
            {
                state = InteractionState.ACTIVE;
                Activated?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                InfoTextDisplay.Display(infoTxt, 2);
                ResetCurrent();
            }
        }
    }

    private void OnDisconnected(Actor actor)
    {
        ResetCurrent();
    }

    private void ResetCurrent()
    {
        state = InteractionState.INACTIVE;
        Current?.OnDisconnectEvent.RemoveListener(OnDisconnected);
        Current = null;
        Deactivated?.Invoke(this, EventArgs.Empty);
    }

    private void FixedUpdate()
    {
        if (state == InteractionState.ACTIVE)
        {
            if (Current == null || !Current.isInUse)
                ResetCurrent();
            else
                return;
        }
        Interactable interactable = GetClosestInteractable();
        if (interactable == null || interactable != Nearest)
            Nearest?.Deselect(owner);
        Nearest = interactable;
        if (Nearest != null)
        {
            state = InteractionState.INACTIVE;
            interactable.Select(owner);
        }
    }

    private Interactable GetClosestInteractable()
    {
        Interactable best = null;
        float lowestScore = float.MaxValue;

        if (nearbyInteractables == null)
            nearbyInteractables = new List<Interactable>();

        for (int i = 0; i < nearbyInteractables.Count; i++)
        {
            Interactable current = nearbyInteractables[i];
            if (!current.IsEnabled)
                continue;

            if (current == null)
            {
                nearbyInteractables.RemoveAt(i);
                continue;
            }

            float distance = Vector3.Distance(transform.position, current.transform.position);
            distance /= triggerEventListener.radius;

            Vector3 dirToObject = (current.transform.position - Camera.main.transform.position);
            dirToObject.Normalize();

            float dot = -Vector3.Dot(Camera.main.transform.forward, dirToObject);

            float score = distance + dot;

            if (score < lowestScore)
            {
                lowestScore = score;
                best = current;
            }
        }

        return best;
    }

    private void OnDestroy()
    {
        Reset();
    }

    private void OnDrawGizmos()
    {
        if (nearbyInteractables == null)
            return;

        foreach (var ia in nearbyInteractables)
        {
            Gizmos.color = ia == Current ? Color.red : Color.cyan;
            Gizmos.DrawLine(transform.position, ia.transform.position);
        }
    }

    public void Reset()
    {
        if (Current != null)
            Break();
    }
}

