﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PickupReaction : Reaction
{
    public Item item;
    public int amount = 1;
    public bool destroyUponPickup = true;

    public override bool CanInteract(Actor actor, out string infoMsg)
    {
        bool canInteract = base.CanInteract(actor, out infoMsg);
        if (canInteract)
        {
            InventoryComponent inventory = actor.GetComponent<InventoryComponent>();
            canInteract &= inventory.CanAddItem(item);
            if (!canInteract) infoMsg = "Inventory full!";
        }
        return canInteract;
    }
    
    public void SetData(string itemId, int amount)
    {
        photonView.RPC("SetAmountRpc", RpcTarget.All, itemId, amount);
    }

    [PunRPC]
    private void SetAmountRpc(string itemId, int amount)
    {
        this.item = ScavGameManager.Instance.gameSettings.itemCollection.Find(itemId);
        this.amount = amount;
    }
    
    protected override void OnInteract(Actor actor)
    {
        base.OnInteract(actor);

        InventoryComponent inventory = actor.GetComponent<InventoryComponent>();
        inventory.AddItemByID(item.AssetID, amount);
        interactable.Disconnect();
        if (destroyUponPickup)
        {
            photonView.TransferOwnership(actor.photonView.Owner);
            photonView.RPC("DestroyRpc", RpcTarget.All);
        }
    }

    [PunRPC]
    private void DestroyRpc()
    {
        interactable.IsEnabled = false;
        MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
        foreach (var r in renderers)
        {
            r.enabled = false;
        }
        interactable.GetComponent<Rigidbody>().isKinematic = true;
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach (var c in colliders)
        {
            c.enabled = false;
        }
        if (photonView.IsMine)
        {

            //PhotonNetwork.Destroy(gameObject);
        }
    }
}
