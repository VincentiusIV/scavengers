﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

public class TakeItemReaction : Reaction
{
    public int Progress => (int)(((float)Mathf.Min(taken, targetAmount) / (float)targetAmount) * 100.0f);

    public Item item;
    public bool takeAll = true;
    public int targetAmount = 10;

    [Header("Debug")]
    public int taken = 0;

    protected override void OnInteract(Actor actor)
    {
        base.OnInteract(actor);

        InventoryComponent inventory = actor.GetComponent<InventoryComponent>();
        int amount = inventory.GetTotalAmountOfItem(item.AssetID);

        if(amount > 0)
        {
            inventory.RemoveItemByID(item.AssetID, amount);
            photonView.RPC("AddTakenAmount", RpcTarget.All, amount);
            interactable.Disconnect();
        }
        else
        {
            InfoTextDisplay.Display(string.Format("No {0} in inventory...", item.AssetName));
            interactable.Disconnect();
        }
    }

    [PunRPC]
    private void AddTakenAmount(int amount)
    {
        taken += amount;
    }

    public void ResetTaken()
    {
        photonView.RPC("ResetTakenRpc", RpcTarget.All);
    }

    [PunRPC]
    private void ResetTakenRpc()
    {
        taken = 0;
    }
}
