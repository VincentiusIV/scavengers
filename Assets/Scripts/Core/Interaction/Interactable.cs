﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(Rigidbody))]
public class Interactable : MonoBehaviour
{
    public UnityEvent<Actor> OnSelectEvent, OnDeselectEvent, OnInteractEvent, OnDisconnectEvent;
    public bool IsEnabled { get; set; } = true;
    public bool isInUse, lockActor = true, instantDisconnect = false;
    private Actor user;
    protected PhotonView photonView;
    protected Reaction[] reactions;

    protected virtual void Awake()
    {
        photonView = GetComponent<PhotonView>();
        reactions = GetComponents<Reaction>();
    }

    public void Select(Actor actor)
    {
        OnSelected(actor);
    }

    public void Deselect(Actor actor)
    {
        OnDeselected(actor);
    }

    public virtual bool CanInteract(Actor actor, out string infoMsg)
    {
        infoMsg = "";
        if(isInUse)
        {
            infoMsg = "Already in use";
            return false;
        }
        foreach (var reaction in reactions)
            if (!reaction.CanInteract(actor, out infoMsg))
                return false;
        return true;
    }

    public bool Interact(Actor actor, out string infoMsg)
    {
        infoMsg = "";
        if (!CanInteract(actor, out infoMsg))
            return false;
        user = actor;
        photonView.RPC("SetIsInUse", RpcTarget.All, true);
        isInUse = true;
        if (lockActor)
            actor.CanControl = false;
        
        // TODO: Timer before OnInteract is invoked...
        // Polish: Actor does a searching gesture during that time.
        OnInteract(actor);
        if (instantDisconnect)
            Disconnect();
        return true;
    }

    [PunRPC]
    protected void SetIsInUse(bool state)
    {
        isInUse = state;
    }

    public void Disconnect()
    {
        OnDisconnect(user);
        if (lockActor)
            user.CanControl = true;
        photonView.RPC("SetIsInUse", RpcTarget.All, false);
        user = null;
    }

    protected virtual void OnSelected(Actor actor)
    {
        OnSelectEvent.Invoke(actor);
    }

    protected virtual void OnDeselected(Actor actor)
    {
        OnDeselectEvent.Invoke(actor);
    }

    protected virtual void OnInteract(Actor actor)
    {
        OnInteractEvent.Invoke(actor);
    }

    protected virtual void OnDisconnect(Actor actor)
    {
        OnDisconnectEvent.Invoke(actor);
    }
}
