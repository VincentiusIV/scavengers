﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AdvancedAudioSource))]
public class SoundReaction : Reaction
{
    public AudioClipCollectionSO interactClips;
    public AudioClipCollectionSO disconnectClips;
    private AdvancedAudioSource audioSource;

    protected override void Awake()
    {
        base.Awake();
        audioSource = GetComponent<AdvancedAudioSource>();
    }

    protected override void OnInteract(Actor actor)
    {
        base.OnInteract(actor);
        audioSource.clipCollectionSO = interactClips;
        audioSource.PlayRandom(true);
    }

    protected override void OnDisconnect(Actor actor)
    {
        base.OnDisconnect(actor);
        audioSource.clipCollectionSO = disconnectClips;
        audioSource.PlayRandom(true);
    }
}
