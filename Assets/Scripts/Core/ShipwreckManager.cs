﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class ShipwreckManager : MonoBehaviour
{
    public int distanceTraveled { get; private set; }
    public float normDistance => (float)distanceTraveled / totalDistanceNeeded;

    public UnityEvent OnTravelBegin, OnFTLStart, OnFTLEnd, OnTravelEnd;
    public float initFTLtime = 1, travelTime = 10f, afterFTLtime = 1;
    public string onTravelBeginText, onFTLStartText, onFTLEndText, onTravelEndText;
    public Shipwreck[] shipwrecks;
    public SpaceshipManager shipManager;
    public int totalDistanceNeeded = 10;

    public string onGameSuccessText = "Success", onGameSuccessReasonText = "Sufficient fuel scavenged and reached destination.", onGameFailedText = "Failed", onGameFailReasonText = "Insufficient fuel scavenged.";

    private PhotonView photonView;
    private int currentShipwreckIdx = 0;
    private Shipwreck CurrentShipwreck => shipwrecks[currentShipwreckIdx];
    private IEnumerator travelRoutine;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        Debug.Assert(shipwrecks.Length > 0);  
        foreach (var shipwreck in shipwrecks)
            shipwreck.Exit();
        if (PhotonNetwork.IsMasterClient)
            ScavGameManager.OnMasterGameStart += OnMasterGameStart;
    }

    private void OnMasterGameStart(object sender, EventArgs e)
    {
        GoToFirstShipwreck();
    }

    public void GoToNextShipwreck()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("GoToSceneRpc", RpcTarget.All, currentShipwreckIdx + 1);
        }
    }

    public void GoToFirstShipwreck()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("GoToSceneRpc", RpcTarget.All, 0);
        }
    }

    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftAlt))
        {
            if(Input.GetKeyDown(KeyCode.F1))
            {
                EndGame();
            }
        }
    }

    [PunRPC]
    private void GoToSceneRpc(int idx)
    {
        if (travelRoutine != null)
            StopCoroutine(travelRoutine);
        travelRoutine = TravelRoutine(idx);
        StartCoroutine(travelRoutine);
    }

    private IEnumerator TravelRoutine(int idx)
    {
        SpaceshipManager.Instance.Lock();
        if(currentShipwreckIdx != idx)
        {
            yield return CurrentShipwreck.Exit();
            yield return HandleHyperJump();
        }

        if(PhotonNetwork.IsMasterClient)
        {
            distanceTraveled += SpaceshipManager.Instance.tanker.taken;
            photonView.RPC("SetDistanceTraveledRpc", RpcTarget.All, distanceTraveled);
            SpaceshipManager.Instance.tanker.ResetTaken();
        }

        yield return ContinueOrFinishGame(idx);   
    }

    private IEnumerator ContinueOrFinishGame(int idx)
    {
        if (idx < shipwrecks.Length)
        {
            currentShipwreckIdx = Mathf.Max(0, idx) % shipwrecks.Length;
            TeleportShip(CurrentShipwreck.shipJoint);
            yield return CurrentShipwreck.Enter();

            SpaceshipManager.Instance.Unlock();
            SpaceshipManager.Instance.airlock.Open();
        }
        else
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        foreach (var scav in Scavengers.scavengers)
        {
            scav.LockControl();
        }
        CreditsScreen credits = (CreditsScreen)UIManager.Instance.GetScreen(typeof(CreditsScreen));
        bool success = (distanceTraveled >= totalDistanceNeeded);
        string missionResult = success ? onGameSuccessText : onGameFailedText;
        string reason = success ? onGameSuccessReasonText : onGameFailReasonText;
        credits.HandleCredits(missionResult, reason);
        UIManager.ShowScreen(typeof(CreditsScreen));
    }

    private void ReturnToLobby()
    {
        Debug.Log("Returning to lobby...");
        Application.Quit();
    }

    [PunRPC]
    private void SetDistanceTraveledRpc(int distanceTraveled)
    {
        this.distanceTraveled = distanceTraveled;
    }

    private IEnumerator HandleCredits()
    {
        if(distanceTraveled >= totalDistanceNeeded)
        {
            // success
            Debug.Log("[ScavengersGame]: Succesfully traveled the required distance!");
        }
        else
        {
            // failed
            Debug.Log("[ScavengersGame]: Failed to travel the required distance!");
        }
        yield return null;
    }

    private IEnumerator HandleHyperJump()
    {
        OnTravelBegin.Invoke();
        SpaceshipManager.OnAISpeak.Invoke(onTravelBeginText);
        yield return new WaitForSeconds(initFTLtime);
        OnFTLStart.Invoke();
        SpaceshipManager.OnAISpeak.Invoke(onFTLStartText);
        yield return new WaitForSeconds(travelTime);
        OnFTLEnd.Invoke();
        SpaceshipManager.OnAISpeak.Invoke(onFTLEndText);
        yield return new WaitForSeconds(afterFTLtime);
        OnTravelEnd.Invoke();
        SpaceshipManager.OnAISpeak.Invoke(onTravelEndText);
    }

    private void TeleportShip(Transform shipJoint)
    {
        shipManager.TeleportShip(shipJoint);
    }
}
