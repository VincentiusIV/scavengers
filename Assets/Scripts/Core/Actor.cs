﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Photon.Pun;
using Photon.Pun.UtilityScripts;

public interface IHitable
{
    void Hit();
}

public class Actor : MonoBehaviour, IHitable
{
    public UnityEvent OnLockChanged, OnHit, OnKilled, OnRespawn;
    public bool IsAlive => HP > 0;
    public bool CanControl { get; set; } = true;
    public PhotonView photonView { get; private set; }
    public Animator animator;
    public int maxHP = 3;
    public int HP { get; private set; }
    public float NormHP => (float)HP / maxHP;
    protected Movement move;

    protected virtual void Awake()
    {
        photonView = GetComponent<PhotonView>();
        move = GetComponent<Movement>();
        if (animator == null)
            animator = GetComponentInChildren<Animator>();
        HP = maxHP;
    }
    
    public void LockControl()
    {
        Debug.Log("Locked control");
        CanControl = false;
        OnLockChanged.Invoke();
    }

    public void UnlockControl()
    {
        Debug.Log("UnLocked control");
        CanControl = true;
        OnLockChanged.Invoke();
    }

    public void Hit()
    {
        Debug.LogFormat("[{0}] was hit, remain HP: {1}/{2}", gameObject.name, HP, maxHP);
        --HP;
        OnHit.Invoke();
        if (HP <= 0)
            Kill();
    }

    public void Kill()
    {
        photonView.RPC("KillRpc", RpcTarget.All);
    }

    [PunRPC]
    protected virtual void KillRpc()
    {
        HP = 0;
        UpdateAliveState();
        LockControl();
        OnKilled.Invoke();
    }

    public void Respawn()
    {
        photonView.RPC("RespawnRpc", RpcTarget.All);
    }

    [PunRPC]
    protected virtual void RespawnRpc()
    {
        HP = maxHP;
        UpdateAliveState();
        UnlockControl();
        OnRespawn.Invoke();
    }

    private void UpdateAliveState()
    {
        animator.SetBool("IsDead", !IsAlive);
        GetComponent<Collider>().enabled = IsAlive;
        GetComponent<Rigidbody>().isKinematic = !IsAlive;
    }

}
