﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class Movement : MonoBehaviour
{
    public float velocityToAnimMult = 1.0f,  animSpeedMultiplier = 1f, dampTime = 0.5f;
    public float moveSpeed = 3f, crouchSpeed = 1f, rotateSpeed = 0.6f;
    public bool IsCrouching { get; set; }
    private PhotonView photonView;
    private Vector3 force;
    public Rigidbody Rigidbody { get; private set; }
    protected Animator Animator { get; set; }
    public bool IsGrounded { get; private set; }
    public LayerMask groundLayer;
    public float groundCheckDist = 0.2f;

    private Actor actor;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
        Rigidbody = GetComponent<Rigidbody>();
        Animator = GetComponentInChildren<Animator>();
        actor = GetComponent<Actor>();
    }

    public void Move(Vector3 direction)
    {
        this.force += direction;
    }

    public void SwitchCrouch()
    {
        IsCrouching = !IsCrouching;
    }

    private void FixedUpdate()
    {
        IsGrounded = Physics.Raycast(transform.position + Vector3.up * groundCheckDist / 2, Vector3.down, groundCheckDist, groundLayer);
        if (photonView.IsMine && actor.IsAlive)
        {
            float speed = IsCrouching ? crouchSpeed : moveSpeed;
            Vector3 velocity = transform.TransformVector(force * speed);
            velocity = Vector3.ProjectOnPlane(velocity, Vector3.up);
            velocity.y = Rigidbody.velocity.y;
            Rigidbody.velocity = velocity;
            Rigidbody.angularVelocity = Vector3.zero;
            force = Vector3.zero;
            UpdateAnimator(Rigidbody.velocity);
        }
    }

    private void UpdateAnimator(Vector3 velocity)
    {
        if (Animator.runtimeAnimatorController == null)
            return;
        Vector3 worldVelocity = transform.InverseTransformVector(velocity) * velocityToAnimMult;
        float xVelocity = worldVelocity.x;
        Animator.SetFloat("X_VELOCITY", xVelocity, dampTime, Time.fixedDeltaTime);
        float yVelocity = worldVelocity.y;
        Animator.SetFloat("Y_VELOCITY", yVelocity, dampTime, Time.fixedDeltaTime);
        float zVelocity = worldVelocity.z;
        Animator.SetFloat("Z_VELOCITY", zVelocity, dampTime, Time.fixedDeltaTime);
        Animator.SetBool("IsGrounded", true);
        Animator.SetBool("IsCrouching", IsCrouching);
        Animator.speed = animSpeedMultiplier;
    }

    public void OnAnimatorMove()
    {
        if (Time.deltaTime > 0f)
        {
            Vector3 _velocity = (Animator.deltaPosition * 1f) / Time.deltaTime;
            _velocity.y = Rigidbody.velocity.y;
            Rigidbody.velocity = _velocity;
        }
    }
}
