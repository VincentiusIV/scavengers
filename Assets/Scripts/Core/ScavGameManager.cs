﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public static class ScavengersGame
{
    public const float PLAYER_RESPAWN_TIME = 4.0f;
    public const int PLAYER_MAX_LIVES = 3;
    public const string PLAYER_LIVES = "PlayerLives";
    public const string PLAYER_READY = "IsPlayerReady";
    public const string PLAYER_LOADED_LEVEL = "PlayerLoadedLevel";
    public const string LOBBY_SCENE = "Lobby";
    public static string CURRENT_SCENE;
}

public class ScavGameManager : MonoBehaviourPunCallbacks
{
    public static ScavGameManager Instance = null;
    public static System.EventHandler OnMasterGameStart;
    public Text InfoText;
    public GameSettings gameSettings;
    public Transform[] spawnPositions;
    public GameObject[] playerPrefabs;

    public float invalidDepthThreshold = -100f;

    #region UNITY

    public void Awake()
    {
        Instance = this;
        if (gameSettings == null)
            gameSettings = Resources.Load<GameSettings>("GameSettings");
        foreach (var item in gameSettings.itemCollection.items)
        {
            if (item.Equipable)
                PhotonNetwork.PrefabPool.RegisterPrefab(item.Equipable.name, item.Equipable);
            if (item.PickupObject)
                PhotonNetwork.PrefabPool.RegisterPrefab(item.PickupObject.name, item.PickupObject);
        }
        foreach (var prefab in gameSettings.prefabs)
        {
            if (prefab == null)
                continue;
            PhotonNetwork.PrefabPool.RegisterPrefab(prefab.name, prefab);
        }
    }

    private void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }

    public override void OnEnable()
    {
        base.OnEnable();

        CountdownTimer.OnCountdownTimerHasExpired += OnCountdownTimerIsExpired;
    }

    public void Start()
    {
        Hashtable props = new Hashtable
            {
                {ScavengersGame.PLAYER_LOADED_LEVEL, true}
            };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);

        StartGame();
    }

    public override void OnDisable()
    {
        base.OnDisable();

        CountdownTimer.OnCountdownTimerHasExpired -= OnCountdownTimerIsExpired;
        
    }

    #endregion

    #region COROUTINES

    private IEnumerator EndOfGame(string winner, int score)
    {
        float timer = 5.0f;

        while (timer > 0.0f)
        {
            if (InfoText)
            {
                InfoText.text = string.Format("Player {0} won with {1} points.\n\n\nReturning to login screen in {2} seconds.", winner, score, timer.ToString("n2"));
            }
            yield return new WaitForEndOfFrame();

            timer -= Time.deltaTime;
        }

        PhotonNetwork.LeaveRoom();
    }

    #endregion

    #region PUN CALLBACKS

    public override void OnDisconnected(DisconnectCause cause)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(ScavengersGame.LOBBY_SCENE);
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.Disconnect();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        CheckEndOfGame();
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps)
    {
        if (changedProps.ContainsKey(ScavengersGame.PLAYER_LIVES))
        {
            CheckEndOfGame();
            return;
        }

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        // if there was no countdown yet, the master client (this one) waits until everyone loaded the level and sets a timer start
        int startTimestamp;
        bool startTimeIsSet = CountdownTimer.TryGetStartTime(out startTimestamp);

        if (changedProps.ContainsKey(ScavengersGame.PLAYER_LOADED_LEVEL))
        {
            if (CheckAllPlayerLoadedLevel())
            {
                if (!startTimeIsSet)
                {
                    CountdownTimer.SetStartTime();
                }
            }
            else
            {
                // not all players loaded yet. wait:
                if (InfoText)
                {
                    Debug.Log("setting text waiting for players! ", this.InfoText);
                    InfoText.text = "Waiting for other players...";

                }
                
            }
        }

    }

    #endregion

    private void FixedUpdate()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            foreach (var scav in Scavengers.scavengers)
            {
                if(scav.transform.position.y < invalidDepthThreshold)
                {
                    Transform spawnPos = spawnPositions[PhotonNetwork.LocalPlayer.GetPlayerNumber() % spawnPositions.Length];
                    Rigidbody rb = scav.GetComponent<Rigidbody>();
                    rb.MovePosition(spawnPos.position);
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                }
            }
        }
    }

    public static Vector3 GetSpawnPosition(int index)
    {
        return Instance.spawnPositions[Mathf.Max(0, index) % Instance.spawnPositions.Length].position;
    }

    // called by OnCountdownTimerIsExpired() when the timer ended
    private void StartGame()
    {
        Debug.Log("StartGame!");

        // on rejoin, we have to figure out if the spaceship exists or not
        // if this is a rejoin (the ship is already network instantiated and will be setup via event) we don't need to call PN.Instantiate

        Transform spawnPos = spawnPositions[PhotonNetwork.LocalPlayer.GetPlayerNumber() % spawnPositions.Length];
        string playerPrefabName = playerPrefabs.Length > 0 ? playerPrefabs[PhotonNetwork.LocalPlayer.GetPlayerNumber() % playerPrefabs.Length].name : "Player";
        PhotonNetwork.Instantiate(playerPrefabName, spawnPos.position, spawnPos.rotation, 0);      // avoid this call on rejoin (ship was network instantiated before)
        if (InfoText)
        {
            InfoText.text = "";
        }
        if (PhotonNetwork.IsMasterClient)
        {
            // Start of game systems.
            OnMasterGameStart?.Invoke(this, System.EventArgs.Empty);
            OnMasterGameStart = null;
        }
    }

    private bool CheckAllPlayerLoadedLevel()
    {
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object playerLoadedLevel;

            if (p.CustomProperties.TryGetValue(ScavengersGame.PLAYER_LOADED_LEVEL, out playerLoadedLevel))
            {
                if ((bool)playerLoadedLevel)
                {
                    continue;
                }
            }

            return false;
        }

        return true;
    }

    private void CheckEndOfGame()
    {
        bool allDestroyed = true;

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            object lives;
            if (p.CustomProperties.TryGetValue(ScavengersGame.PLAYER_LIVES, out lives))
            {
                if ((int)lives > 0)
                {
                    allDestroyed = false;
                    break;
                }
            }
        }

        if (allDestroyed)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                StopAllCoroutines();
            }

            string winner = "";
            int score = -1;

            foreach (Player p in PhotonNetwork.PlayerList)
            {
                if (p.GetScore() > score)
                {
                    winner = p.NickName;
                    score = p.GetScore();
                }
            }

            StartCoroutine(EndOfGame(winner, score));
        }
    }

    private void OnCountdownTimerIsExpired()
    {

    }
}