﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnAnimatorMoveTest : MonoBehaviour
{
    public UnityEvent OnAnimatorMoveEvent;

    private void OnAnimatorMove()
    {
        OnAnimatorMoveEvent.Invoke();
    }
}
