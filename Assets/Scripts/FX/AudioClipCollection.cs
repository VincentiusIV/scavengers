﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
public class AudioClipCollection
{
    [SerializeField] private AudioClip[] audioClips = new AudioClip[0];

    public AudioClip[] Clips { get { return audioClips; } }

    public AudioClip Random
    {
        get
        {
            if (audioClips == null || audioClips.Length == 0)
                return null;

            int randIdx = UnityEngine.Random.Range(0, audioClips.Length);
            return audioClips[randIdx];
        }
    }
}
