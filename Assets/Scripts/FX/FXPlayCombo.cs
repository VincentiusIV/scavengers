﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXPlayCombo : MonoBehaviour
{
    public ParticleSystem mainParticleSystem;
    public AdvancedAudioSource advancedAudio;

    public void Play()
    {
        if (mainParticleSystem.isPlaying)
            mainParticleSystem.Stop();
        mainParticleSystem.Play();
        advancedAudio.PlayRandom(true);
    }
}
