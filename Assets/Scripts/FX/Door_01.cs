﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Door_01 : Door {

//	public GameObject Wing_Right;
//	public GameObject Wing_Left;

	public Animation WingLeft;
	public Animation WingRight;
    
    protected override void OpenDoorAnimation()
    {
        base.OpenDoorAnimation();
        GetComponent<AudioSource>().Play();
        WingLeft["door_01_wing_left"].speed = 1;
        WingRight["door_01_wing_right"].speed = 1;
        WingLeft.Play();
        WingRight.Play();
    }

    protected override void CloseDoorAnimation()
    {
        base.CloseDoorAnimation();
        GetComponent<AudioSource>().Play();
        WingLeft["door_01_wing_left"].time = WingLeft["door_01_wing_left"].length;
        WingRight["door_01_wing_right"].time = WingRight["door_01_wing_right"].length;
        WingLeft["door_01_wing_left"].speed = -1;
        WingRight["door_01_wing_right"].speed = -1;
        WingLeft.Play();
        WingRight.Play();
    }
}
