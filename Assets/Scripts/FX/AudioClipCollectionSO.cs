﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = "Audio Clip Collection", menuName = "Audio Clip Collection")]
public class AudioClipCollectionSO : ScriptableObject
{
    public AudioClipCollection clipCollection;
    public float minPitch = 0.8f, maxPitch = 1.2f;
    public float timeBetweenClips = 0.1f;

}
