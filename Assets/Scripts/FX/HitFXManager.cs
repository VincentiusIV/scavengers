﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[System.Serializable]
public enum HitType
{
    DEFAULT,
    BLOOD,
    SKELETON,
    SAND,
    ROCK,
    WOOD,
    LEAVES
}

public class HitFXManager : MonoBehaviour
{
    private static HitFXManager instance;

    [System.Serializable]
    public struct HitFXType
    {
        public HitType hitType;
        public FXPlayCombo fxPrefab;
    }

    public int poolSize = 10;
    public HitFXType[] hitFXTypes;
    private Dictionary<HitType, FXPlayCombo[]> sortedEffects;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        sortedEffects = new Dictionary<HitType, FXPlayCombo[]>();
        foreach (var hitFXType in hitFXTypes)
        {
            if (hitFXType.fxPrefab == null)
                continue;

            sortedEffects.Add(hitFXType.hitType, new FXPlayCombo[poolSize]);
            for (int i = 0; i < poolSize; i++)
            {
                sortedEffects[hitFXType.hitType][i] = Instantiate(hitFXType.fxPrefab.gameObject, transform).GetComponent<FXPlayCombo>();
                sortedEffects[hitFXType.hitType][i].transform.SetParent(transform);
            }
        }
    }

    private FXPlayCombo GetFX(HitType hitType)
    {
        foreach (var fxCombo in sortedEffects[hitType])
        {
            if (!fxCombo.mainParticleSystem.isPlaying)
                return fxCombo;
        }
        return sortedEffects[hitType][0];
    }

    public static void PlayAtPosition(HitType hitType, Vector3 position, Quaternion rotation)
    {
        FXPlayCombo fx = instance.GetFX(hitType);
        fx.transform.position = position;
        fx.transform.rotation = rotation;
        fx.Play();
    }
}
