﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System;

public class BoneSetter : MonoBehaviour
{
    [SerializeField]
    private Transform skeletonRoot;

    [SerializeField]
    private string[] boneNames;

    [SerializeField]
    private Transform[] bones;

    [SerializeField]
    private string hipsRootName = "mixamorig:Hips";

    [SerializeField]
    private SkinnedMeshRenderer[] skins;

    void Awake()
    {
        Setup();
    }

    [ContextMenu("Setup")]
    public void Setup()
    {
        if(skeletonRoot == null)
            skeletonRoot = transform.parent.FindInChildren(hipsRootName);
        GetBoneNames();
        SetBones();
    }
    
    private void GetBoneNames()
    {
        skins = GetComponentsInChildren<SkinnedMeshRenderer>();

        SkinnedMeshRenderer example = skins.FirstOrDefault();
        if (example == null)
            return;
        // Get bone names for the skin.
        Transform[] exampleBones = example.bones;
        boneNames = new string[exampleBones.Length];
        for (int i = 0; i < exampleBones.Length; i++)
        {
            string t = exampleBones[i].name;
            boneNames[i] = t;
        }
    }

    private void SetBones()
    {
        this.bones = new Transform[boneNames.Length];
        for (int i = 0; i < boneNames.Length; i++)
            this.bones[i] = skeletonRoot.FindInChildren(boneNames[i]);
        foreach (var skin in skins)
        {
            skin.bones = this.bones;
            skin.rootBone = skeletonRoot;
        }
    }

    [ContextMenu("Map Avatars")]
    public void MapAvatars()
    {
        Avatar avatar = GetComponentInParent<Animator>().avatar;


        Debug.Log("ayaya");
    }
}