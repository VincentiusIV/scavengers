﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    public float speed = 5f;

    public bool x, y = true, z;

    private void Update()
    {
        Vector3 euler = transform.eulerAngles;
        euler.x += (x ? 1f : 0f) * (speed * Time.deltaTime);
        euler.y += (y ? 1f : 0f) * (speed * Time.deltaTime);
        euler.z += (z ? 1f : 0f) * (speed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(euler);
    }
}
