﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Door_02 : Door {

	public Animation Wing;
    
    protected override void OpenDoorAnimation()
    {
        base.OpenDoorAnimation();
        GetComponent<AudioSource>().Play();
        Wing["door_02_wing"].speed = 1;
        Wing.Play();
    }

    protected override void CloseDoorAnimation()
    {
        base.CloseDoorAnimation();
        GetComponent<AudioSource>().Play();
        Wing["door_02_wing"].time = Wing["door_02_wing"].length;
        Wing["door_02_wing"].speed = -1;
        Wing.Play();
    }
}
