﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


[RequireComponent(typeof(PhotonView))]
public abstract class Door : MonoBehaviour
{
    private PhotonView photonView;
    public bool autoOpenForPlayers = false;
    public bool IsLocked = false;
    private bool isOpen;
    private int counter;

    protected virtual void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }
    
    void OnTriggerEnter(Collider c)
    {
        Actor actor = c.GetComponent<Actor>();
        if (actor != null)
        {
            if (actor.GetType() == typeof(Scavengers) && !autoOpenForPlayers)
                return;
            Shade shade = actor as Shade;
            if (shade != null && !shade.canOpenDoors)
                return;
            ++counter;
            Open();
        }
    }

    public void Switch()
    {
        if (isOpen)
            Close();
        else
            Open();
    }

    public void Open()
    {
        if (!isOpen)
            photonView.RPC("OpenDoor", RpcTarget.All);
    }

    [PunRPC]
    protected void OpenDoor()
    {
        if (isOpen || IsLocked)
            return;
        isOpen = true;
        OpenDoorAnimation();
    }

    protected virtual void OpenDoorAnimation()
    {

    }

    void OnTriggerExit(Collider c)
    {

        Actor actor = c.GetComponent<Actor>();
        if (actor != null)
        {
            if (actor.GetType() == typeof(Scavengers) && !autoOpenForPlayers)
                return;
            Shade shade = actor as Shade;
            if (shade != null && !shade.canOpenDoors)
                return;
            --counter;
            if (counter <= 0)
            {
                counter = 0;
            }
        }
    }

    public void Close()
    {
        if (isOpen && counter == 0)
            photonView.RPC("CloseDoor", RpcTarget.All);
    }

    [PunRPC]
    protected void CloseDoor()
    {
        if (!isOpen)
            return;
        isOpen = false;
        CloseDoorAnimation();
    }

    public void Lock()
    {
        photonView.RPC("SetLockRpc", RpcTarget.All, true);
    }

    public void Unlock()
    {
        photonView.RPC("SetLockRpc", RpcTarget.All, false);
    }

    [PunRPC]
    protected void SetLockRpc(bool value)
    {
        IsLocked = value;
    }

    protected virtual void CloseDoorAnimation()
    {

    }
}
