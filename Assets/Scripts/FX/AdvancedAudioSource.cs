﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AdvancedAudioSource : MonoBehaviour
{
    public AudioClipCollection clipCollection { get => clipCollectionSO?.clipCollection; }
    public AudioClipCollectionSO clipCollectionSO;
    public bool playRandomLoop = false;
    public bool playRandomAwake = false;
    public AudioSource AudioSource { get; private set; }
    public bool IsLoopActive { get; set; }
    public AudioSource[] audioBuffers;
    private Queue<AudioClip> clipQueue = new Queue<AudioClip>();
    private float timeInactive;

    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
        clipQueue = new Queue<AudioClip>();
    }

    private void Start()
    {
        if (playRandomAwake)
            PlayRandom();
    }

    public void StartLoop()
    {
        IsLoopActive = true;
    }

    public void StopLoop()
    {
        IsLoopActive = false;
    }

    public void PlayRandom(bool overrideCurrent = false)
    {
        if (clipCollectionSO == null)
            return;
        IsLoopActive = playRandomLoop;
        AudioSource sourceToUse = AudioSource;
        if (audioBuffers.Length > 0)
            sourceToUse = GetFreeSource();

        if (sourceToUse.isPlaying && !overrideCurrent)
        {
            return;
        }

        float pitch = Random.Range(clipCollectionSO.minPitch, clipCollectionSO.maxPitch);

        sourceToUse.pitch = pitch;
        sourceToUse.clip = clipCollection?.Random;
        sourceToUse.Play();
    }

    public void Play(AudioClip clip, bool overrideCurrent = false, bool addToQueue = false)
    {
        AudioSource sourceToUse = AudioSource;
        if (audioBuffers.Length > 0)
            sourceToUse = GetFreeSource();

        if (sourceToUse.isPlaying)
        {
            if (addToQueue)
                clipQueue.Enqueue(clip);
            if(!overrideCurrent)
                return;
        }

        sourceToUse.pitch = (clipCollectionSO.minPitch + clipCollectionSO.maxPitch) / 2f;
        sourceToUse.clip = clip;
        sourceToUse.Play();
    }

    private AudioSource GetFreeSource()
    {
        for (int i = 0; i < audioBuffers.Length; i++)
        {
            if (!audioBuffers[i].isPlaying)
                return audioBuffers[i];
        }
        return AudioSource;
    }

    private void LateUpdate()
    {
        if (AudioSource.isPlaying)
            timeInactive = 0;
        else
            timeInactive += Time.deltaTime;

        if (clipQueue.Count > 0 && !AudioSource.isPlaying)
        {
            Play(clipQueue.Dequeue(), true, false);
        }
        else if (IsLoopActive && playRandomLoop && timeInactive >= clipCollectionSO.timeBetweenClips && !AudioSource.isPlaying)
        {
            PlayRandom(true);
            timeInactive = 0;
        }
    }
}
