﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movement))]
public class FootstepFX : MonoBehaviour
{
    public AdvancedAudioSource walkFootstepAudioSource;
    public AdvancedAudioSource crouchFootstepAudioSource;
    public float distanceBetweenWalkStep = 1;

    private Movement movement;
    private Vector3 lastFootstepPosition;
    private float distanceTraveled = 0;

    private void Awake()
    {
        movement = GetComponent<Movement>();
    }

    private void Update()
    {
        if(movement.IsGrounded)
        {
            Vector3 currentPos = transform.position;
            currentPos.y = 0;
            distanceTraveled += Vector3.Distance(currentPos, lastFootstepPosition);
            lastFootstepPosition = currentPos;
            if (distanceTraveled > distanceBetweenWalkStep)
            {
                distanceTraveled = 0f;
                if (movement.IsCrouching)
                    crouchFootstepAudioSource.PlayRandom(true);
                else
                    walkFootstepAudioSource.PlayRandom(true);
            }
        }        
    }
}
