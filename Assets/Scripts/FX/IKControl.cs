﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class IKControl : MonoBehaviour
{
    public Rig aimRig;
    public float transitionTime = 1f;
    private IEnumerator lerpRoutine;

    private void Start()
    {
        SetAimRig(false);
    }

    public void SetAimRig(bool state)
    {
        if (lerpRoutine != null)
            StopCoroutine(lerpRoutine);
        if (state)
            lerpRoutine = LerpRigWeight(aimRig, 1f);
        else
            lerpRoutine = LerpRigWeight(aimRig, 0f);
        if (gameObject.activeInHierarchy)
            StartCoroutine(lerpRoutine);
        else
            aimRig.weight = state ? 1f : 0f;
    }

    private IEnumerator LerpRigWeight(Rig rig, float targetWeigth)
    {
        float timer = 0f;
        float startWeight = rig.weight;
        while(timer < transitionTime)
        {
            rig.weight = Mathf.Lerp(startWeight, targetWeigth, timer/transitionTime);
            timer += Time.deltaTime;
            yield return null;
        }
        rig.weight = targetWeigth;
    }
}
