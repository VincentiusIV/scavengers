﻿using UnityEngine;
using System.Collections;

public class ObjectFacingCamera : MonoBehaviour
{
	private Camera camera;

	void LateUpdate()
	{
        if (camera == null)
        {
            camera = Camera.main;
            return;
        }
		transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward, camera.transform.rotation * Vector3.up);
	}
}